import { Component, OnInit } from "@angular/core";

declare interface RouteInfo {
  path: string;
  title: string;
  activity: String;
  icon: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    icon: "analytics",
    activity: "Tableau de bord"
  },
  {
    path:"/home/cattle",
    title:"home cattle",
    icon:"savings",
    activity:"Elevage"
  },
  {
    path:"/home/fish",
    title:"home fish",
    icon: "set_meal",
    activity: "Pisciculture"
  },
  {
    path: "/home/farming",
    title: "Speculation",
    icon: "local_florist",
    activity: "Culture"
  },
  {
    path: "/home/poultry",
    title: "Volaille",
    icon: "flutter_dash",
    activity: "Avicluture"
  },
  {
    path: "/home/commerce",
    title: "Commerce",
    icon: "shopping_cart",
    activity: "Commerce"
  },
  {
    path: "/home/rh",
    title: "RH",
    icon: "assignment_ind",
    activity: "RH"
  }
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
