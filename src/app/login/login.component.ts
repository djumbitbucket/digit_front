import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { LoggerValue } from '../../commons/loggerValue';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form = {
    username: "",
    password: ""
  };
  private loggerValue! : LoggerValue;
  invalidLogin = false
  constructor(private router: Router,
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    var mainPanel = document.getElementsByClassName('main-panel')[0];
    var body = document.getElementsByTagName('body')[0];
    mainPanel.setAttribute('data','green');
    body.classList.add('white-content');
  }

  onSubmit(form: { username: string; password: string; }) {
    
    (this.loginservice.authenticate(form.username,form.password).subscribe(
      data => {
        this.loggerValue = data;
        this.showToken();
        this.router.navigate(['accueil'])
        this.invalidLogin = false
      },
      error => {
        this.invalidLogin = true
      }
    )
    );
  }

  showToken(){
    console.log(this.loggerValue.token);
    if(this.loggerValue.token && this.loggerValue.firstname && this.loggerValue.lastname && this.loggerValue.role){
    sessionStorage.setItem('token',this.loggerValue.token)
    sessionStorage.setItem('firstname',this.loggerValue.firstname);
    sessionStorage.setItem('lastname',this.loggerValue.lastname);
    sessionStorage.setItem('role',this.loggerValue.role)
    }
  }
}

