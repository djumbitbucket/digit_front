import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit{
  title = "black-dashboard-angular";

  ngOnInit(){
    this.changeDashboardColor();
  }

  changeDashboardColor(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('white-content');
  }
}
