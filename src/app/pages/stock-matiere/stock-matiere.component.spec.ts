import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockMatiereComponent } from './stock-matiere.component';

describe('StockMatiereComponent', () => {
  let component: StockMatiereComponent;
  let fixture: ComponentFixture<StockMatiereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockMatiereComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockMatiereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
