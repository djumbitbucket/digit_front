import { Component, OnInit } from '@angular/core';
import { IncomingStock } from 'src/commons/IncomingStock';
import { Product } from 'src/commons/product';
import { MatiereService } from 'src/services/matiere.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-stock-matiere',
  templateUrl: './stock-matiere.component.html',
  styleUrls: ['./stock-matiere.component.scss']
})
export class StockMatiereComponent implements OnInit {


  public products: Product[];
  public p:number = 1;
  public page:number = 1;
  public incomingStocks: IncomingStock[] = [];
  public formTemplate: boolean = false;
  public currentView: number = 1; //{producte: 1, incomingStock: 2}
  public selectedProduct: Product = null;
  public form: Product = new Product();
  public formStock: IncomingStock = new IncomingStock();

  public error: string = "";

  public createXorUpdate: any = {
    product: true,
    incomingStock: true
  };

  constructor(private service: MatiereService){}

  ngOnInit(){
    this.getProducts()
  }

  getProducts(){
    this.service.getProducts().subscribe(
      data => this.products = data
    )
  }

  getStock(produit: Product){
    this.selectedProduct = produit;
    this.changeView(2);
    this.service.getIncomingStockByMatiere(produit.id).subscribe(data => this.incomingStocks = data);
  }

  onSubmit(){
    if (this.createXorUpdate.product) {
      this.service.postProduct(this.form).subscribe(
        data => this.handleResponseProduct(data, true)
      )
    } else {
      this.service.updateProduct(this.form).subscribe(
        data => this.handleResponseProduct(data, false)
      )
    }
    this.createXorUpdate.product = true;
  }

  handleResponseProduct(data:any, saveMode: boolean){
      if(data.success){
        let produit: Product = data.product;
        if (saveMode) {
          this.products.push(produit);
        } else {
          const idx = this.products.findIndex(item => item.id == produit.id);
          this.products[idx] = produit;
        }
        this.resetForm();
        this.activeFormTemplate(false);
      }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
      }
  }

  onSubmitStock(){
    if (this.createXorUpdate.incomingStock) {
      this.formStock.product = this.selectedProduct;
      this.service.postIncomingStock(this.formStock).subscribe(
        data => this.handleResponseIncomingStock(data, true)
      )
    } else {
      console.log('updating', this.formStock)
      this.service.updateIncomingStock(this.formStock).subscribe(
        data => this.handleResponseIncomingStock(data, false)
      )
    }
    this.createXorUpdate.incomingStock = true;
  }

  handleResponseIncomingStock(data:any, saveMode: boolean){
      if(data.success){
        let stock: IncomingStock = data.stock;
        if (saveMode) {
          this.incomingStocks.push(stock);
        } else {
          const idx = this.incomingStocks.findIndex(item => item.id == stock.id);
          this.incomingStocks[idx] = stock;
        }
      }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
      }
      this.resetFormIncomingStock();
      this.activeFormTemplate(false);
  }

  onDelete(id:number){
    Swal.fire({
      title: 'Are you sure?',
      text: "Voulez vous supprimer cet enregistrement?",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteProduct(id).subscribe(
          data => {
            this.products = this.products.filter(item => item.id != id);
            Swal.fire('Deleted!', 'Supprimé avec succès.', 'success')
          }
        )
      }
    })
  }

  onDeleteStock(id:number){
    Swal.fire({
      title: 'Are you sure?',
      text: "Voulez vous supprimer cet enregistrement?",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteIncomingStock(id).subscribe(
          data => {
            this.incomingStocks = this.incomingStocks.filter(item => item.id != id);
            Swal.fire('Deleted!', 'Supprimé avec succès.', 'success')
          }
        )
      }
    })
  }

  editing(produit: Product){
    this.formTemplate = true;
    this.editForm(produit);
    this.createXorUpdate.product = false;
    window.scroll(0, 0);
  }

  editingStock(stock: IncomingStock){
    this.formTemplate = true;
    this.editFormIncomingStock(stock);
    this.createXorUpdate.incomingStock = false;
    window.scroll(0, 0);
  }

  changeView(viewNumber: number){
    this.currentView = viewNumber;
  }

  activeFormTemplate(value: boolean){
    this.formTemplate = value;
  }
  
  goBack(){
    if (this.currentView === 2) {
      this.changeView(1);
    }
  }

  cancel(){
    this.activeFormTemplate(false);
    this.resetForm();
    this.createXorUpdate.product = true;
  }

  cancelStock(){
    this.activeFormTemplate(false);
    this.resetFormIncomingStock();
    this.createXorUpdate.incomingStock = true;
  }

  resetForm(){
    this.form = new Product();
  }

  resetFormIncomingStock(){
    this.formStock = new IncomingStock();
  }

  editForm(produit: Product){
    this.form.edit(produit);
  }

  editFormIncomingStock(stock: IncomingStock){
    stock.product = this.selectedProduct;
    this.formStock.edit(stock);
  }

}
