import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { CalendarySpeculation } from "src/commons/calendarySpeculation";
import { Speculation } from "src/commons/speculation";
import { SpeculationService } from "src/services/speculation.service";
import { Seed } from "src/commons/seed";
import { Planting } from "src/commons/planting";
import { MinSpeculation } from "src/commons/minSpeculation";
import Swal from "sweetalert2";
import { Harvest } from "src/commons/harvest";


@Component({
    selector: 'app-speculation',
    templateUrl: 'speculation.component.html'
})
export class SpeculationComponent implements OnInit{

    public seeds: Seed[];
    public speculations: Speculation[];
    public plantings: Planting[];
    public minSpeculation: MinSpeculation[];
    public calendarys: CalendarySpeculation[];
    public harvests: Harvest[];
    public selectedSpeculation: Speculation = null;
    public selectedSeed: Seed = null;
    public p:number = 1;
    public page:number = 1;
    public currentView: number = 1; //{seed: 1, speculation: 2, minSpeculation: 3, calendar: 4, harvest: 5}
    public formTemplate: boolean = false;
    
    public form: Speculation = new Speculation();
    
    public formCalendar: CalendarySpeculation = new CalendarySpeculation();

    public formSeed: Seed = new Seed();

    public formMin: MinSpeculation = new MinSpeculation();
    
    public formHarvest: Harvest = new Harvest(); 
    
    public error: string = "";

    public createXorUpdate: any = {
      seed: true,
      speculation: true,
      minCalendar: true,
      calendar: true,
      harvest: true
    }

    public harvestReasons: any = [
      {label: 'Vendre', value: 'sale', hasValue: true},
      {label: 'Pourri', value: 'die', hasValue: false},
      {label: 'Stocquer', value: 'stock', hasValue: false},
    ];

    constructor(private service: SpeculationService){}

    ngOnInit(){
      this.getSeed();
      this.getPlanting();
    }

    getSpeculation(seed: Seed){
      this.selectedSeed = seed;
      this.changeView(2);
      this.service.getSpeculationBySeed(seed.id).subscribe(
        data => this.speculations = data
      )
    }

    getSeed(){
      this.service.getSeed().subscribe(
          data => this.seeds = data
      )
    }

    getPlanting(){
      this.service.getPlanting().subscribe(
        data => this.plantings = data
      )
    }

    getMinSpecultion(seed:Seed){
      this.selectedSeed = seed;
      this.changeView(3);
      this.service.getMinCalendaryByCategory(this.selectedSeed.id).subscribe(
        data => {
          this.minSpeculation = data;
        }
      )
    }

    getCalendary(speculation: Speculation){
      this.selectedSpeculation = speculation;
      this.changeView(4);
      this.service.getCalendary(this.selectedSpeculation.id).subscribe(
        data => this.calendarys = data
      )
    }

    getHarvest(speculation: Speculation){
      this.selectedSpeculation = speculation;
      this.changeView(5);
      this.service.getHarvest(this.selectedSpeculation.id).subscribe(
        data => this.harvests = data
      )
    }

    onSubmit(){
      if (this.createXorUpdate.speculation) {
        this.form.seed = this.selectedSeed;
        this.service.postSpeculation(this.form).subscribe(
          data => this.handleResponseSpeculation(data, true)
        )
      } else {
        this.service.updateSpeculation(this.form).subscribe(
          data => this.handleResponseSpeculation(data, false)
        )
      }
      this.createXorUpdate.speculation = true;
    }

    handleResponseSpeculation(data:any, saveMode: boolean){
        if(data.success){
          let speculation: Speculation = data.speculation;
          if (saveMode) {
            this.speculations.push(speculation);
          } else {
            const idx = this.speculations.findIndex(item => item.id == speculation.id);
            this.speculations[idx] = speculation;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
          console.log("Some thimg is wrong");
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
        }
    }

    onSubmitSeed(){
      if (this.createXorUpdate.seed) {
        this.service.postSeed(this.formSeed).subscribe(
          data => this.handleResponseSeed(data, true)
        )
      } else {
        this.service.updateSeed(this.formSeed).subscribe(
          data => this.handleResponseSeed(data, false)
        )
      }
      this.createXorUpdate.seed = true;
    }

    handleResponseSeed(data:any, saveMode: boolean){
        if(data.success){
          let seed: Seed = data.seed;
          if (saveMode) {
            this.seeds.push(seed);
          } else {
            const idx = this.seeds.findIndex(item => item.id == seed.id);
            this.seeds[idx] = seed;
          }
          this.resetFormSeed();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    onSubmitCalendar(f:NgForm){
      if (this.createXorUpdate.calendar) {
        this.formCalendar.speculation = this.selectedSpeculation;
        this.formCalendar.name = this.formCalendar.calendaryName;
        this.service.postCalendary2(this.formCalendar).subscribe(
          data => {
           // console.log('created calendar', data);
            this.calendarys = this.calendarys.concat(data['calendars']);
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }else{
        this.service.updateCalendar(this.formCalendar).subscribe(
          data => {
            let calendar: CalendarySpeculation = data['calendar'];
            const idx = this.calendarys.findIndex(item => item.id == calendar.id);
            this.calendarys[idx] = calendar;
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }
      this.createXorUpdate.calendar = true;
    }

    onSubmitMin(){
      if (this.createXorUpdate.minCalendar) {
        this.formMin.seed = this.selectedSeed;
        this.service.postMin(this.formMin).subscribe(
          data => {
            this.minSpeculation.push(data);
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      } else {
        this.service.updateMinCalendar(this.formMin).subscribe(
          data => {
            let minCal: MinSpeculation = data['minCalendar'];
            const idx = this.minSpeculation.findIndex(item => item.id == minCal.id);
            this.minSpeculation[idx] = minCal;
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      }
      this.createXorUpdate.minCalendar = true;
    }

    onSubmitHarvest(){
      if (this.createXorUpdate.harvest) {
        this.formHarvest.speculation = this.selectedSpeculation;
        this.service.postHarvest(this.formHarvest).subscribe(
          data => this.handleResponseHarvest(data, true)
        )
      } else {
        this.service.updateHarvest(this.formHarvest).subscribe(
          data => this.handleResponseHarvest(data, false)
        )
      }
      this.createXorUpdate.harvest = true;
    }

    handleResponseHarvest(data: any, saveMode: boolean){
        if(data.success){
          let harvest: Harvest = data.harvest;
          if (saveMode) {
            this.harvests.push(harvest);
          } else {
            const idx = this.harvests.findIndex(item => item.id == harvest.id);
            this.harvests[idx] = harvest;
          }
          this.resetFormHarvest();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    isNonValueReason(){
      const nonValueCases: string[] = ['die', 'stock'];
      return nonValueCases.includes(this.formHarvest.raison);
    }

    reasonChange(){
      if (this.isNonValueReason()) {
        this.formHarvest.valeur = 0;
      }
    }

    getRaisonLabel(value: string){
      let raison: string = null;
      this.harvestReasons.forEach(elem => {
        if (elem.value === value) {
          raison = elem.label;
        }
      });
      return raison;
    }

      onDelete(id:number){
        Swal.fire({
          title: 'Are you sure?',
          text: "Voulez vous supprimer cet enregistrement?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.service.deleteSpeculation(id).subscribe(
              data => {
                this.speculations = this.speculations.filter(item => item.id != id);
                Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
              }
            )
          }
        })
      }

      onDeleteSeed(id:number){
        Swal.fire({
          title: 'Are you sure?',
          text: "Voulez vous supprimer cet enregistrement?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.service.deleteSeed(id).subscribe(
              data => {
                this.seeds = this.seeds.filter(item => item.id != id);
                Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
              }
            )
          }
        })
      }

      onDeleteCalendar(id:number){
        Swal.fire({
          title: 'Are you sure?',
          text: "Voulez vous supprimer cet enregistrement?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.service.deleteCalendar(id).subscribe(
              data => {
                console.log(data);
                this.calendarys = this.calendarys.filter(item => item.id != id);
                Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
              }
            )
          }
        })
      }

      onDeleteMinCalendar(id:number){
        Swal.fire({
          title: 'Are you sure?',
          text: "Voulez vous supprimer cet enregistrement?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.service.deleteMinCalendar(id).subscribe(
              data => {
                console.log(data);
                this.minSpeculation = this.minSpeculation.filter(item => item.id != id);
                Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
              }
            )
          }
        })
      }

      onDeleteHarvest(id:number){
        Swal.fire({
          title: 'Are you sure?',
          text: "Voulez vous supprimer cet enregistrement?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.service.deleteHarvest(id).subscribe(
              data => {
                console.log(data);
                this.harvests = this.harvests.filter(item => item.id != id);
                Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
              }
            )
          }
        })
      }      

      editingSeed(seed: Seed){
        this.formTemplate = true;
        this.editFormSeed(seed);
        this.createXorUpdate.seed = false;
        window.scroll(0, 0);
      }
  
      editingSpeculation(speculation: Speculation){
        this.formTemplate = true;
        this.editFormSpeculation(speculation);
        this.createXorUpdate.speculation = false;
        window.scroll(0, 0);
      }
  
      editingCalendar(calendar: CalendarySpeculation){
        this.formTemplate = true;
        this.createXorUpdate.calendar = false;
        this.editFormCalendar(calendar);
        window.scroll(0, 0);
      }
  
      editingMinCalendar(minSpeculation: MinSpeculation){
        this.formTemplate = true;
        this.editFormMin(minSpeculation);
        this.createXorUpdate.minCalendar = false;
        window.scroll(0, 0);
      }
  
      editingHarvest(harvest: Harvest){
        this.formTemplate = true;
        this.editFormHarvest(harvest);
        this.createXorUpdate.harvest = false;
        window.scroll(0, 0);
      }

      changeView(viewNumber: number){
        this.currentView = viewNumber;
      }
    
      activeFormTemplate(value: boolean){
        this.formTemplate = value;
      }
      
      goBack(){
        if (this.currentView === 2 || this.currentView === 3) {
          this.changeView(1);
        } else if (this.currentView === 4 || this.currentView === 5) {
          this.changeView(2);
        }
      }

      cancelSeed(){
        this.activeFormTemplate(false);
        this.resetFormSeed();
        this.createXorUpdate.seed = true;
      }
  
      cancelSpeculation(){
        this.activeFormTemplate(false);
        this.resetForm();
        this.createXorUpdate.speculation = true;
      }
  
      cancelCalendar(){
        this.activeFormTemplate(false);
        this.resetFormCalendar();
        this.createXorUpdate.calendar = true;
      }
  
      cancelMinCalendar(){
        this.activeFormTemplate(false);
        this.resetFormMin();
        this.createXorUpdate.minCalendar = true;
      }
  
      cancelHarvest(){
        this.activeFormTemplate(false);
        this.resetFormHarvest();
        this.createXorUpdate.harvest = true;
      }

      resetForm(){
        this.form = new Speculation();
      }

      resetFormCalendar(){
        this.formCalendar = new CalendarySpeculation();
      }
  
      resetFormSeed(){
        this.formSeed = new Seed();
      }
  
      resetFormMin(){
        this.formMin = new MinSpeculation();
      }

      resetFormHarvest(){
        this.formHarvest = new Harvest();
      }

      editFormSeed(seed: Seed){
        this.formSeed.edit(seed);
      }
  
      editFormSpeculation(speculation: Speculation){
        speculation.seed = this.selectedSeed;
        speculation.planting = this.plantings.filter(item => item.name === speculation.plantingName)[0];
        this.form.edit(speculation);
      }
  
      editFormCalendar(calendar: CalendarySpeculation){
        this.formCalendar.edit(calendar);
        this.formCalendar.speculation = this.selectedSpeculation;
      }
  
      editFormMin(minSpec: MinSpeculation){
        minSpec.seed = this.selectedSeed;
        this.formMin.edit(minSpec);
      }

      editFormHarvest(harvest: Harvest){
        harvest.speculation = this.selectedSpeculation;
        this.formHarvest.edit(harvest);
      }
      
}