import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCommerceComponent } from './home-commerce.component';

describe('HomeCommerceComponent', () => {
  let component: HomeCommerceComponent;
  let fixture: ComponentFixture<HomeCommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeCommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
