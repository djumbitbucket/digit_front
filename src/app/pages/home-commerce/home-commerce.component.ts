import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-commerce',
  templateUrl: './home-commerce.component.html',
  styleUrls: ['./home-commerce.component.scss']
})
export class HomeCommerceComponent {

  public viewCommerce: string = 'x';

  onChangeView(val: string){
      this.viewCommerce = val;
  }

}
