import { isNull } from "@angular/compiler/src/output/output_ast";
import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { CalendaryPoultry } from "src/commons/calendaryPoultry";
import { Category } from "src/commons/category";
import { CategoryPoultry } from "src/commons/CategoryPoultry";
import { ChickenCoop } from "src/commons/chickenCoop";
import { Egg } from "src/commons/egg";
import { MinPoultry } from "src/commons/minPoultry";
import { OutPoultry } from "src/commons/outPoultry";
import { Poultry } from "src/commons/poultry";
import { PoultryService } from "src/services/poultry.service";
import Swal from "sweetalert2";


@Component({
    selector:"app-poultry",
    templateUrl: "poultry.component.html"
})
export class PoultryComponent implements OnInit{

    public poultrys: Poultry[];
    public chickenCoops: ChickenCoop[];
    public calendarys: CalendaryPoultry[];
    public categorys: CategoryPoultry[];
    public minPoultrys: MinPoultry[];
    public outPoultrys: OutPoultry[];
    public eggs: Egg[];
    selectedCategory: CategoryPoultry = null;
    selectedPoultry: Poultry = null;
    p:number = 1;
    page:number = 1;

    public currentView: number = 1; //{ctegory: 1, poultry: 2, minPoultry: 3, calendar: 4, outPoultry: 5, egg: 6}
    formTemplate: boolean = false;

    public form: Poultry = new Poultry();

    public formCalendar: CalendaryPoultry = new CalendaryPoultry();

    public formCategory: CategoryPoultry = new CategoryPoultry();

    public formMin: MinPoultry = new MinPoultry();

    public formOutPoultry: OutPoultry = new OutPoultry();

    public formEgg: Egg = new Egg();

    public error: string = "";

    public createXorUpdate: any = {
      category: true,
      poultry: true,
      minCalendar: true,
      calendar: true,
      outPoultry: true ,
      egg: true
    }

    public categoryTypes: any = [
      {label: 'CHAIRE', value: 'pulpit'},
      {label: 'OEUF FECONDE', value: 'fertil'},
      {label: 'OEUF NON FECONDE', value: 'not_fertil'},
    ];

    public outReasons: any = [
      {label: 'Vendre', value: 'sale', hasValue: true},
      {label: 'Pourri', value: 'die', hasValue: false},
      {label: 'Stocquer', value: 'stock', hasValue: false},
    ];

    constructor(private service: PoultryService){}

    ngOnInit(): void{
      this.getCategory();
      this.getChickenCoop()
    }

    getData(){
      this.service.getData().subscribe(
          data => this.poultrys = data
      )
    }

    getChickenCoop(){
        this.service.getChickenCoops().subscribe(
            data => {
              this.chickenCoops = data ; //console.log(data)
            }
        )
    }

    onSubmit(){
      if (this.createXorUpdate.poultry) {
        this.form.initQuantity = this.form.quantity; //when creating
        this.form.category = this.selectedCategory;
        this.service.postPoultry(this.form).subscribe(
          data => this.handleResponsePoultry(data, true)
        )
      } else {
        this.service.updatePoultry(this.form).subscribe(
          data => this.handleResponsePoultry(data, false)
        )
      }
      this.createXorUpdate.poultry = true;
    }

    handleResponsePoultry(data:any, saveMode: boolean){
        if(data.success){
          let poultry: Poultry = data.poultry;
          if (saveMode) {
            this.poultrys.push(poultry);
          } else {
            const idx = this.poultrys.findIndex(item => item.id == poultry.id);
            this.poultrys[idx] = poultry;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
          console.log("Some thimg is wrong");
        }
    }

    onSubmitCalendar(f:NgForm){
      if (this.createXorUpdate.calendar) {
        this.formCalendar.poultry = this.selectedPoultry;
        this.formCalendar.name = this.formCalendar.calendaryName;
        this.service.postCalendary2(this.formCalendar).subscribe(
          data => {
            //console.log('calendary poultry', data);
            this.calendarys = this.calendarys.concat(data['calendars']);
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      } else {
        this.service.updateCalendar(this.formCalendar).subscribe(
          data => {
            let calendar: CalendaryPoultry = data['calendar']; 
            const idx = this.calendarys.findIndex(item => item.id == calendar.id);
            this.calendarys[idx] = calendar;
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }
      this.createXorUpdate.calendar = true;
    }

    onSubmitCategory(){
      if (this.createXorUpdate.category) {
        this.service.postCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, true)
        )
      } else {
        this.service.updateCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, false)
        )
      }
      this.createXorUpdate.category = true;
    }

    handleResponseCategory(data:any, saveMode: boolean){
      if(data.success){
        let category: CategoryPoultry = data.category;
        if (saveMode) {
          this.categorys.push(category);
        } else {
          const idx = this.categorys.findIndex(item => item.id == category.id);
          this.categorys[idx] = category;
        }
        this.resetFormCategory();
        this.activeFormTemplate(false);
      }else{
        this.error = "Some thimg is wrong";
        setTimeout(() => { this.error = ""; }, 2000);
        console.log(data);
      }
    }

    onSubmitMin(){
      if (this.createXorUpdate.minCalendar) {
        this.formMin.category = this.selectedCategory;
        this.service.postMinPoultry(this.formMin).subscribe(
          data => {
            this.minPoultrys.push(data);
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      } else {
        this.service.updateMinCalendar(this.formMin).subscribe(
          data => {
            let minCal: MinPoultry = data['minCalendar'];
            const idx = this.minPoultrys.findIndex(item => item.id == minCal.id);
            this.minPoultrys[idx] = minCal;
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      }
      this.createXorUpdate.minCalendar = true;
    }
    
    onSubmitOutPoultry(){
      if (this.createXorUpdate.outPoultry) {
        this.formOutPoultry.poultry = this.selectedPoultry;
        this.service.postOutPoultry(this.formOutPoultry).subscribe(
          data => this.handleResponseOut(data, true)
        )
      } else {
        this.service.updateOutPoultry(this.formOutPoultry).subscribe(
          data => this.handleResponseOut(data, false)
        )
      }
      this.createXorUpdate.outPoultry = true;
    }

    handleResponseOut(data:any, saveMode: boolean){
        if(data.success){
          let out: OutPoultry = data.outPoultry;
          if (saveMode) {
            this.outPoultrys.push(out);
          } else {
            const idx = this.outPoultrys.findIndex(item => item.id == out.id);
            this.outPoultrys[idx] = out;
          }
          this.resetFormOutPoultry();
          this.activeFormTemplate(false);
        }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
        }
    }

    onSubmitEgg(){
      if (this.createXorUpdate.egg) {
        this.formEgg.poultry = this.selectedPoultry;
        this.service.postEgg(this.formEgg).subscribe(
          data => this.handleResponseEgg(data, true)
        )
      } else {
        this.service.updateEgg(this.formEgg).subscribe(
          data => this.handleResponseEgg(data, false)
        )
      }
      this.createXorUpdate.egg = true;
    }

    handleResponseEgg(data:any, saveMode: boolean){
        if(data.success){
          //console.log('saved egg',  data);
          let egg: Egg = data.egg;
          if (saveMode) {
            this.eggs.push(egg);
          } else {
            const idx = this.eggs.findIndex(item => item.id == egg.id);
            this.eggs[idx] = egg;
          }
          this.resetFormEgg();
          this.activeFormTemplate(false);
        }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
        }
    }

    isNonValueReason(){
      const nonValueCases: string[] = ['die', 'stock'];
      return nonValueCases.includes(this.formOutPoultry.raison);
    }

    reasonChange(){
      if (this.isNonValueReason()) {
        this.formOutPoultry.valeur = 0;  //die, stock
      }
      else{
        this.formOutPoultry.valeur = null;  //sale
      }
    }

    getCategory(){
      this.service.getCategory().subscribe(
        data => this.categorys = data
      )
    }

    getPoultry(cat: CategoryPoultry){
      this.selectedCategory = cat;
      this.changeView(2);
      this.service.getPoultryByCategory(this.selectedCategory.id).subscribe(
        data => this.poultrys = data
      )
    }

    getCalendary(poultry: Poultry){
      this.selectedPoultry = poultry;
      this.changeView(4);
      this.service.getCalendaryPoultry(poultry.id).subscribe(
        data => this.calendarys = data
      )
    }

    getMinPoultry(cat: CategoryPoultry){
      this.selectedCategory = cat;
      this.changeView(3);
      this.service.getMinCalendaryByCategory(this.selectedCategory.id).subscribe(
        data => {
          this.minPoultrys = data;
        }        
      )
    }

    getOutPoultry(poultry: Poultry){
      this.selectedPoultry = poultry;
      this.changeView(5);
      this.service.getOutPoultry(this.selectedPoultry.id).subscribe(
        data => this.outPoultrys = data
      )
    }

    getEggs(poultry: Poultry){
      this.selectedPoultry = poultry;
      this.changeView(6);
      this.service.getEgg(this.selectedPoultry.id).subscribe(
        data => this.eggs = data
      )
    }

    eggExists(){
      return this.selectedCategory.type === 'oeuf feconde' || this.selectedCategory.type === 'oeuf non feconde';
    }

    showCategoryType(category: CategoryPoultry){
      return this.categoryTypes.find(x => x.value === category.type).label;
    }

    getRaisonLabel(value: string){
      let raison: string = null;
      this.outReasons.forEach(elem => {
        if (elem.value === value) {
          raison = elem.label;
        }
      });
      return raison;
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deletePoultry(id).subscribe(
            data => {
              this.poultrys = this.poultrys.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Supprimé avec succès.', 'success')
            }
          )
        }
      })
    }

    onDeleteCategory(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCategory(id).subscribe(
            data => {
              this.categorys = this.categorys.filter(item => item.id != id);
              Swal.fire(
                'Deleted!', 'Supprimé avec succès.', 'success')
            }
          )
        }
      })
    }

    onDeleteCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCalendar(id).subscribe(
            data => {
              console.log(data);
              this.calendarys = this.calendarys.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Supprimé avec succès.', 'success')
            }
          )
        }
      })
    }

    onDeleteMinCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteMinCalendar(id).subscribe(
            data => {
              console.log(data);
              this.minPoultrys = this.minPoultrys.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Supprimé avec succès.', 'success')
            }
          )
        }
      })
    }

    onDeleteOutPoultry(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteOutPoultry(id).subscribe(
            data => {
              console.log(data);
              this.outPoultrys = this.outPoultrys.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Supprimé avec succès.', 'success')
            }
          )
        }
      })
    }

    onDeleteEgg(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteEgg(id).subscribe(
            data => {
              console.log(data);
              this.eggs = this.eggs.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Supprimé avec succès.', 'success')
            }
          )
        }
      })
    }

    editingCategory(category: CategoryPoultry){
      this.formTemplate = true;
      this.editFormCategory(category);
      this.createXorUpdate.category = false;
      window.scroll(0, 0);
    }

    editingPoultry(poultry: Poultry){
      this.formTemplate = true;
      this.editFormPoultry(poultry);
      this.createXorUpdate.poultry = false;
      window.scroll(0, 0);
    }

    editingCalendar(calendar: CalendaryPoultry){
      this.formTemplate = true;
      this.createXorUpdate.calendar = false;
      this.editFormCalendar(calendar);
      window.scroll(0, 0);
    }

    editingMinCalendar(minPoultry: MinPoultry){
      this.formTemplate = true;
      this.editFormMin(minPoultry);
      this.createXorUpdate.minCalendar = false;
      window.scroll(0, 0);
    }
  
    editingOutPoultry(outPoultry: OutPoultry){
      this.formTemplate = true;
      this.editFormOutPoultry(outPoultry);
      this.createXorUpdate.outPoultry = false;
      window.scroll(0, 0);
    }

    editingEgg(egg: Egg){
      this.formTemplate = true;
      this.editFormEgg(egg);
      this.createXorUpdate.egg = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2 || this.currentView === 3) {
        this.changeView(1);
      } else if (this.currentView === 4 || this.currentView === 5 || this.currentView === 6) {
        this.changeView(2);
      }
    }

    cancelCategory(){
      this.activeFormTemplate(false);
      this.resetFormCategory();
      this.createXorUpdate.category = true;
    }

    cancelPoultry(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate.poultry = true;
    }

    cancelCalendar(){
      this.activeFormTemplate(false);
      this.resetFormCalendar();
      this.createXorUpdate.calendar = true;
    }

    cancelMinCalendar(){
      this.activeFormTemplate(false);
      this.resetFormMin();
      this.createXorUpdate.minCalendar = true;
    }

    cancelOutPoultry(){
      this.activeFormTemplate(false);
      this.resetFormOutPoultry();
      this.createXorUpdate.outPoultry = true;
    }

    cancelEgg(){
      this.activeFormTemplate(false);
      this.resetFormEgg();
      this.createXorUpdate.egg = true;
    }

    resetForm(){
      this.form = new Poultry();
    }

    resetFormCalendar(){
      this.formCalendar = new CalendaryPoultry();
    }

    resetFormCategory(){
      this.formCategory = new CategoryPoultry();
    }

    resetFormMin(){
      this.formMin = new MinPoultry();
    }

    resetFormOutPoultry(){
      this.formOutPoultry = new OutPoultry();
    }

    resetFormEgg(){
      this.formEgg = new Egg();
    }

    editFormCategory(category: CategoryPoultry){
      this.formCategory.edit(category);
    }

    editFormPoultry(poultry: Poultry){
      poultry.category = this.selectedCategory;
      poultry.chickenCoop = this.chickenCoops.filter(item => item.name === poultry.coopsName)[0];
      this.form.edit(poultry);
    }

    editFormCalendar(calendar: CalendaryPoultry){
      this.formCalendar.edit(calendar);
      this.formCalendar.poultry = this.selectedPoultry;
    }

    editFormMin(minPoultry: MinPoultry){
      minPoultry.category = this.selectedCategory;
      this.formMin.edit(minPoultry);
    }

    editFormOutPoultry(outPoultry: OutPoultry){
      outPoultry.poultry = this.selectedPoultry;
      this.formOutPoultry.edit(outPoultry);
    }

    editFormEgg(egg: Egg){
      this.formEgg.edit(egg);
      this.formEgg.poultry = this.selectedPoultry;
    }
    
}