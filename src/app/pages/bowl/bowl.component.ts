import { NullTemplateVisitor } from "@angular/compiler";
import { Component, OnInit } from "@angular/core";
import { Bowl } from "src/commons/bowl";
import { Fish } from "src/commons/fish";
import { BowlService } from "src/services/bowl.service";
import Swal from "sweetalert2";

@Component({
    selector: 'app-bowl',
    templateUrl: 'bowl.component.html'
})

export class BowlComponent implements OnInit{

    public bowls:Bowl[];
    public fishs:Fish[];
    public selectedBowl: Bowl;
    public p:number = 1;
    public page:number = 1;
    public currentView: number = 1; //{bowl: 1, fish: 2}
    public formTemplate: boolean = false;
    public form: Bowl = new Bowl();

    public error: string = "";

    public createXorUpdate: boolean = true;

    constructor(private service: BowlService){}

    ngOnInit(){
      this.getBowl()
    }

    getBowl(){
      this.service.getBowl().subscribe(
        data => this.bowls = data
      )
    }

    getFish(bowl:Bowl){
      this.selectedBowl = bowl;
      this.changeView(2);
      this.service.getFishByBowl(this.selectedBowl.id).subscribe(
          data => this.fishs = data
      )
    }

    onSubmit(){
      if (this.createXorUpdate) {
        this.service.postBowl(this.form).subscribe(
          data => this.handleResponseBowl(data, true)
        )
      } else {
        this.service.updateBowl(this.form).subscribe(
          data => this.handleResponseBowl(data, false)
        )
      }
      this.createXorUpdate = true;
    }

    handleResponseBowl(data:any, saveMode: boolean){
        if(data.success){
          let bowl: Bowl = data.bowl;
          if (saveMode) {
            this.bowls.push(bowl);
          } else {
            const idx = this.bowls.findIndex(item => item.id == bowl.id);
            this.bowls[idx] = bowl;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteBowl(id).subscribe(
            data => {
              this.bowls = this.bowls.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingBowl(bowl: Bowl){
      this.formTemplate = true;
      this.editForm(bowl);
      this.createXorUpdate = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2) {
        this.changeView(1);
      }
    }

    cancelBowl(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate = true;
    }

    resetForm(){
      this.form = new Bowl();
    }

    editForm(bowl: Bowl){
      this.form.edit(bowl);
    }
}