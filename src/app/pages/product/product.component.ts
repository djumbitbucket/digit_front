import { Component, OnInit } from "@angular/core";
import { Speculation } from "src/commons/speculation";
import { Product } from "src/commons/product";
import { ProductService } from "src/services/product.service";

@Component({
    selector: 'app-product',
    templateUrl: 'product.component.html'
})
export class ProductComponent implements OnInit{

    public products: Product[];
    public farmings: Speculation[];
    selectedProduct = 0;
    p:number = 1;
    page:number = 1;
    activeTemplate: boolean = true;
    formTemplate: boolean = false;
    public form: Product = new Product();

    constructor(private service: ProductService){}

    ngOnInit(){
        this.getData();
    }

    getData(){
        this.service.getData().subscribe(
            data => this.products = data
        )
    }

    onSubmit(){
        this.service.postProduct(this.form).subscribe(
            data => {
                this.products.push(data);
                this.activeFormTemplate();
                this.resetForm();
            }
        )
    }

    getFarming(id:number){
        this.selectedProduct = id;
        this.activeTemplate = false;
        this.service.getFarming(id).subscribe(
          data => this.farmings = data
        )
      }

      goBack(){
        this.activeTemplate = true;
      }
  
      activeFormTemplate(){
        this.formTemplate = !this.formTemplate;
      }

      resetForm(){
          this.form.productName = null;
      }
}