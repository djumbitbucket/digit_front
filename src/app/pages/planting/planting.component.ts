import { Component, OnInit } from "@angular/core";
import { Planting } from "src/commons/planting";
import { Speculation } from "src/commons/speculation";
import { Tree } from "src/commons/tree";
import { PlantingService } from "src/services/planting.service";
import Swal from "sweetalert2";

@Component({
    selector: 'app-planting',
    templateUrl: 'planting.component.html'
})

export class PlantingComponent implements OnInit{

    public plantings:Planting[];
    public speculations: Speculation[];
    public trees: Tree[];
    public p:number = 1;
    public page: number = 1;
    public selectedPlanting: Planting = null;
    public currentView: number = 1; //{planting: 1, speculation: 2, tree: 3}
    public formTemplate: boolean = false;

    public form: Planting = new Planting();
    
    public error: string = "";

    public createXorUpdate: boolean = true;

    constructor(private service:PlantingService){}

    ngOnInit(){
        this.getPlanting();
    }

    onSubmit(){
      if (this.createXorUpdate) {
        this.service.postPlanting(this.form).subscribe(
          data => this.handleResponsePlanting(data, true)
        )
      } else {
        this.service.updatePlanting(this.form).subscribe(
          data => this.handleResponsePlanting(data, false)
        )
      }
      this.createXorUpdate = true;
    }

    handleResponsePlanting(data:any, saveMode: boolean){
        if(data.success){
          let planting: Planting = data.planting;
          if (saveMode) {
            this.plantings.push(planting);
          } else {
            const idx = this.plantings.findIndex(item => item.id == planting.id);
            this.plantings[idx] = planting;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    getPlanting(){
      this.service.getPlanting().subscribe(
          data => this.plantings = data
      )
    }

    getSpeculation(planting:Planting){
      this.selectedPlanting = planting;
      this.changeView(2);
      this.service.getSpeculationByPlanting(this.selectedPlanting.id).subscribe(
        data => {
          this.speculations = data;
        }
      );        
    }

    getTree(planting:Planting){
      this.selectedPlanting = planting;
      this.changeView(3);
      this.service.getTreeByPlanting(this.selectedPlanting.id).subscribe(
        data => {
          this.trees = data;
        }
      );
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deletePlanting(id).subscribe(
            data => {
              this.plantings = this.plantings.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingPlanting(planting: Planting){
      this.formTemplate = true;
      this.editForm(planting);
      this.createXorUpdate = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2 || this.currentView === 3) {
        this.changeView(1);
      }
    }

    cancelPlanting(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate = true;
    }

    resetForm(){
      this.form = new Planting();
    }

    editForm(planting: Planting){
      this.form.edit(planting);
    }

}