import { Component } from "@angular/core";

@Component({
    selector: 'app-homeCattle',
    templateUrl: 'homeCattle.component.html'
})

export class HomeCattleComponent{
    public viewCattle = true;

    onChangeView(val:boolean){
        this.viewCattle = val;
    }
}