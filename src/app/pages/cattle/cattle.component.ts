import { identifierModuleUrl } from "@angular/compiler";
import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { first } from "rxjs";
import { CalendaryCattle } from "src/commons/calendaryCattle";
import { CategoryCattle } from "src/commons/categoryCattle";
import { Cattle } from "src/commons/cattle";
import { Enclosure } from "src/commons/enclosure";
import { MinCattle } from "src/commons/minCattle";
import { CattleService } from "src/services/cattle.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-cattle",
  templateUrl: "cattle.component.html"
})
export class CattleComponent implements OnInit {

    enclosures: Enclosure[];
    cattles: Cattle[];
    calendarys: CalendaryCattle[];
    categorys: CategoryCattle[];
    minCattles: MinCattle[];
    selectedCattle: Cattle = null;
    selectedCategory: CategoryCattle = null;
    p:number = 1;
    page:number = 1;
    formTemplate: boolean = false;
    public currentView: number = 1; //{ctegory: 1, cattle: 2, minCattle: 3, calendar: 4}

    public form: Cattle = new Cattle();

    public formCalendar: CalendaryCattle = new CalendaryCattle();

    public formCategory: CategoryCattle = new CategoryCattle();

    public formMin: MinCattle = new MinCattle();

    public error: string = "";

    public createXorUpdate: any = {
      category: true,
      cattle: true,
      minCalendar: true,
      calendar: true
    }

    constructor(private service: CattleService){}

    ngOnInit(): void {
        this.getEnclosure();
        this.getCategory();
        this.count();
    }
    byName(name:string){
      this.service.getByName(name).subscribe(
        data => console.log(data)
        
      )
    }
    count(){
      this.service.countCattle().subscribe(
        data => console.log(data)
        
      )
    }

    onSubmit(){
      if (this.createXorUpdate.cattle) {
        this.form.category = this.selectedCategory;
        this.service.postCattle(this.form).subscribe(
          data => this.handleResponseCattle(data, true)
        )
      } else {
        this.service.updateCattle(this.form).subscribe(
          data => this.handleResponseCattle(data, false)
        )
      }
      this.createXorUpdate.cattle = true;
    }

    handleResponseCattle(data:any, saveMode: boolean){
        if(data.success){
          if (saveMode) {
            let cattle: Cattle = data.cattle;
            this.cattles.push(cattle);
          } else {
            let cattle = data.cattles;
            const idx = this.cattles.findIndex(item => item.id == cattle.id);
            this.cattles[idx] = cattle;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
        }
    }

    onSubmitCalendar(f:NgForm){
      if (this.createXorUpdate.calendar) {
        this.formCalendar.cattle = this.cattles.filter(item => item.id == this.selectedCattle.id)[0];
        this.service.postCalendaryCattle(this.formCalendar).subscribe(
          data => {
            let calendar: CalendaryCattle = data;
            this.calendarys.push(calendar);
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      } else {
        this.service.updateCalendar(this.formCalendar).subscribe(
          data => {
            let calendar: CalendaryCattle = data['calendar']; 
            const idx = this.calendarys.findIndex(item => item.id == calendar.id);
            this.calendarys[idx] = calendar;
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }
      this.createXorUpdate.calendar = true;
    }

    onSubmitCategory(){
      if (this.createXorUpdate.category) {
        this.service.postCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, true)
        )
      } else {
        this.service.updateCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, false)
        )
      }
      this.createXorUpdate.category = true;
    }

    handleResponseCategory(data:any, saveMode: boolean){
        if(data.success){
          let category: CategoryCattle = data.category;
          if (saveMode) {
            this.categorys.push(category);
          } else {
            const idx = this.categorys.findIndex(item => item.id == category.id);
            this.categorys[idx] = category;
          }
          this.resetFormCategory();
          this.activeFormTemplate(false);
        }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
        }
    }

    onSubmitMin(){
      if (this.createXorUpdate.minCalendar) {
        this.formMin.category = this.selectedCategory;
        this.service.postMinCattle(this.formMin).subscribe(
          data => {
            let minCattle: MinCattle = data;
            this.minCattles.push(minCattle);
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      } else {
        this.service.updateMinCalendar(this.formMin).subscribe(
          data => {
            let minCattle: MinCattle = data['minCalendar'];
            const idx = this.minCattles.findIndex(item => item.id == minCattle.id);
            this.minCattles[idx] = minCattle;
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      }
      this.createXorUpdate.minCalendar = true;
    }

    getData(){
      this.service.getCattle().subscribe(
        data => this.cattles = data
      )
    }

    getEnclosure(){
      this.service.getEnclosure().subscribe(
        data => this.enclosures = data
      )
    }

    getCategory(){
      this.service.getCategory().subscribe(
        data => this.categorys = data
      )
    }

    getCalendary(cattle: Cattle){
      this.selectedCattle = cattle;
      this.changeView(4);
      this.service.getCalendaryCattle(cattle.id).subscribe(
        data => this.calendarys = data
      )
    }

    getCattle(C:CategoryCattle){
      this.selectedCategory = C;
      this.changeView(2);
      this.service.getCattleByCategory(this.selectedCategory.id).subscribe(
        data => this.cattles = data
      )

    }

    getMinCattle(cat:CategoryCattle){
      this.selectedCategory = cat;
      this.changeView(3);
      this.service.getMinCalendaryByCategory(this.selectedCategory.id).subscribe(
        data => {
          this.minCattles = data;
        }        
      )
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCattle(id).subscribe(
            data => {
              this.cattles = this.cattles.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteCategory(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCategory(id).subscribe(
            data => {
              this.categorys = this.categorys.filter(item => item.id != id);
              Swal.fire(
                'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCalendar(id).subscribe(
            data => {
              console.log(data);
              this.calendarys = this.calendarys.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteMinCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteMinCalendar(id).subscribe(
            data => {
              console.log(data);
              this.minCattles = this.minCattles.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingCategory(category: CategoryCattle){
      this.formTemplate = true;
      this.editFormCategory(category);
      this.createXorUpdate.category = false;
      window.scroll(0, 0);
    }

    editingCattle(cattle: Cattle){
      this.formTemplate = true;
      this.editFormCattle(cattle);
      this.createXorUpdate.cattle = false;
      window.scroll(0, 0);
    }

    editingCalendar(calendar: CalendaryCattle){
      this.formTemplate = true;
      this.editFormCalendar(calendar);
      this.createXorUpdate.calendar = false;
      window.scroll(0, 0);
    }

    editingMinCalendar(minCattle: MinCattle){
      this.formTemplate = true;
      this.editFormMin(minCattle);
      this.createXorUpdate.minCalendar = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2 || this.currentView === 3) {
        this.changeView(1);
      } else if (this.currentView === 4) {
        this.changeView(2);
      }
    }

    cancelCategory(){
      this.activeFormTemplate(false);
      this.resetFormCategory();
      this.createXorUpdate.category = true;
    }

    cancelCattle(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate.cattle = true;
    }

    cancelCalendar(){
      this.activeFormTemplate(false);
      this.resetFormCalendar();
      this.createXorUpdate.calendar = true;
    }

    cancelMinCalendar(){
      this.activeFormTemplate(false);
      this.resetFormMin();
      this.createXorUpdate.minCalendar = true;
    }

    resetForm(){
      this.form = new Cattle();
    }

    resetFormCalendar(){
      this.formCalendar = new CalendaryCattle();
    }

    resetFormCategory(){
      this.formCategory = new CategoryCattle();
    }

    resetFormMin(){
      this.formMin = new MinCattle();
    }

    editFormCategory(category: CategoryCattle){
      this.formCategory.edit(category);
    }

    editFormCattle(cattle: Cattle){
      cattle.category = this.selectedCategory;
      cattle.enclosure = this.enclosures.filter(item => item.name === cattle.enclosureName)[0];
      this.form.edit(cattle);
    }

    editFormCalendar(calendar: CalendaryCattle){
      this.formCalendar.edit(calendar);
      this.formCalendar.cattle = this.selectedCattle;
    }

    editFormMin(minCattle: MinCattle){
      this.formMin.edit(minCattle);
      this.formMin.category = this.selectedCategory;
    }

}