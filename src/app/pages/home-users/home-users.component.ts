import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-users',
  templateUrl: './home-users.component.html',
  styleUrls: ['./home-users.component.scss']
})
export class HomeUsersComponent {
  public view = "x";

  onChangeView(val:string){
      this.view = val;
  }
}
