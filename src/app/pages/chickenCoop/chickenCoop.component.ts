import { Component, OnInit } from "@angular/core";
import { ChickenCoop } from "src/commons/chickenCoop";
import { Poultry } from "src/commons/poultry";
import { ChickenCoopService } from "src/services/chickenCoop.service";
import Swal from "sweetalert2";

@Component({
    selector: "app-chickenCoop",
    templateUrl: "chickenCoop.component.html"
})

export class ChickenCoopComponent implements OnInit{

    selectedCoops: ChickenCoop = null;
    p:number = 1;
    page:number = 1;
    public currentView: number = 1; //{chickenCoop: 1, poultry: 2}
    formTemplate: boolean = false;
    public poultrys: Poultry[];
    public chickenCoops: ChickenCoop[];
    public form: ChickenCoop = new ChickenCoop();

    public error: string = "";

    public createXorUpdate: boolean = true;

    constructor(private service:ChickenCoopService){}

    ngOnInit(): void{
            this.getData();
    }

    getData(){
        this.service.getData().subscribe(
            data => this.chickenCoops = data
        )
    }

    onSubmit(){
        if (this.createXorUpdate) {
          this.service.postChickenCoop(this.form).subscribe(
            data => this.handleResponseChickenCoop(data, true)
          )
        } else {
          this.service.updateChickenCoop(this.form).subscribe(
            data => this.handleResponseChickenCoop(data, false)
          )
        }
        this.createXorUpdate = true;
    }

    handleResponseChickenCoop(data:any, saveMode: boolean){
      console.log(data)
        if(data.success){
          let chickenCoop: ChickenCoop = data.chickenCoop;
          if (saveMode) {
            this.chickenCoops.push(chickenCoop);
          } else {
            const idx = this.chickenCoops.findIndex(item => item.id == chickenCoop.id);
            this.chickenCoops[idx] = chickenCoop;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    getPoultry(coop:ChickenCoop){
      this.selectedCoops = coop;
      this.changeView(2);
      this.service.getPoultry(this.selectedCoops.id).subscribe(
        data => this.poultrys = data
      )
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteChickenCoops(id).subscribe(
            data => {
              this.chickenCoops = this.chickenCoops.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

      editingChickenCoop(chickenCoop: ChickenCoop){
        this.formTemplate = true;
        this.editForm(chickenCoop);
        this.createXorUpdate = false;
        window.scroll(0, 0);
      }

      changeView(viewNumber: number){
        this.currentView = viewNumber;
      }
    
      activeFormTemplate(value: boolean){
        this.formTemplate = value;
      }
      
      goBack(){
        if (this.currentView === 2) {
          this.changeView(1);
        }
      }

      cancelChickenCoop(){
        this.activeFormTemplate(false);
        this.resetForm();
        this.createXorUpdate = true;
      }

      resetForm(){
        this.form = new ChickenCoop();
      }

      editForm(chickenCoop: ChickenCoop){
        this.form.edit(chickenCoop);
      }
    
}