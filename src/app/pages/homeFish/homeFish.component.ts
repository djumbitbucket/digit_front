import { Component } from "@angular/core";

@Component({
    selector: 'app-homeFish',
    templateUrl: 'homeFish.component.html'
})

export class HomeFishComponent{
    public viewFish = true;

    onChangeView(val:boolean){
        this.viewFish = val;
    }
}