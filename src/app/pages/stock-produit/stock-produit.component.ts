import { NullTemplateVisitor } from "@angular/compiler";
import { Component, OnInit } from "@angular/core";
import { Bowl } from "src/commons/bowl";
import { Fish } from "src/commons/fish";
import { OutgoingStock } from "src/commons/OutgoingStock ";
import { BowlService } from "src/services/bowl.service";
import { StockService } from "src/services/stock.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-stock-produit',
  templateUrl: './stock-produit.component.html',
  styleUrls: ['./stock-produit.component.scss']
})
export class StockProduitComponent implements OnInit {

  public stocks: any = [];
  public outGoingStocks: OutgoingStock[];
  public p:number = 1;
  public page:number = 1;
  //public activeTemplate: boolean = true; //replaced by changeView
  public formTemplate: boolean = false;
  public currentView: number = 1; //{stock: 1, details: 2}
  public selectedProduit: string = null;
  public form: OutgoingStock = new OutgoingStock();

  public error: string = "";

  public createXorUpdate: boolean = true;

  constructor(private service: StockService){}

  ngOnInit(){
      this.getStocks()
  }

  getStocks(){
    this.service.getStockProduits().subscribe(
      data => this.stocks = data
    )
  }

  getOutGoingStocks(produit: string){
    this.selectedProduit = produit;
    this.form.produit = produit;
    this.changeView(2);
    this.service.getOutgoingStockByStockProduit(produit).subscribe(data => this.outGoingStocks = data);
  }
  
  getSorties(produit: string){
    this.changeView(3);
  }

  onSubmit(){
    if (this.createXorUpdate) {
      this.service.postOutGoingStock(this.form).subscribe(
        data => this.handleResponseOutgoingStock(data, true)
      )
    } else {
      this.service.updateOutGoingStock(this.form).subscribe(
        data => this.handleResponseOutgoingStock(data, false)
      )
    }
    this.createXorUpdate = true;
  }

  handleResponseOutgoingStock(data:any, saveMode: boolean){
      if(data.success){
        let out: OutgoingStock = data.stock;
        if (saveMode) {
          this.outGoingStocks.push(out);
        } else {
          const idx = this.outGoingStocks.findIndex(item => item.id == out.id);
          this.outGoingStocks[idx] = out;
        }
        this.resetForm();
        this.activeFormTemplate(false);
      }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
      }
  }

  canDelete(obj: OutgoingStock){
    return obj.type === 'out';
  }

  onDelete(id:number){
    Swal.fire({
      title: 'Are you sure?',
      text: "Voulez vous supprimer cet enregistrement?",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteOutGoingStock(id).subscribe(
          data => {
            this.outGoingStocks = this.outGoingStocks.filter(item => item.id != id);
            Swal.fire('Deleted!', 'Supprimé avec succès.', 'success')
          }
        )
      }
    })
  }

  editing(stock: OutgoingStock){
    this.formTemplate = true;
    this.editForm(stock);
    this.createXorUpdate = false;
    window.scroll(0, 0);
  }

  changeView(viewNumber: number){
    this.currentView = viewNumber;
  }

  activeFormTemplate(value: boolean){
    this.formTemplate = value;
  }
  
  goBack(){
    if (this.currentView === 2) {
      this.changeView(1);
    }
  }

  cancel(){
    this.activeFormTemplate(false);
    this.resetForm();
    this.createXorUpdate = true;
  }

  resetForm(){
    this.form = new OutgoingStock();
    this.form.produit = this.selectedProduit;
  }

  editForm(stock: OutgoingStock){
    stock.produit = this.selectedProduit;
    this.form.edit(stock);
  }

}
