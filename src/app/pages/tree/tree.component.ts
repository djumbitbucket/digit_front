import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { CalendaryTree } from "src/commons/calendaryTree";
import { Category } from "src/commons/category";
import { MinTree } from "src/commons/minTree";
import { Planting } from "src/commons/planting";
import { Tree } from "src/commons/tree";
import { TreeService } from "src/services/tree.service";
import Swal from "sweetalert2";

@Component({
    selector: 'app-tree',
    templateUrl: 'tree.component.html'
})

export class TreeComponent implements OnInit{

    public categorys: Category[];
    public trees: Tree[];
    public selectedTree: Tree = null;
    public selectedCategory: Category = null;
    public p:number = 1;
    public page:number = 1;
    public currentView: number = 1; //{ctegory: 1, tree: 2, minTree: 3, calendar: 4}
    public formTemplate: boolean = false;
    public calendars:CalendaryTree[];
    public minTree: MinTree[];
    public plantings: Planting[];

    public form: Tree = new Tree();

    public formCategory: Category = new Category();
    
    public formCalendar: CalendaryTree = new CalendaryTree();
    
    public formMin: MinTree = new MinTree();
    
    public error: string = "";

    public createXorUpdate: any = {
      category: true,
      tree: true,
      minCalendar: true,
      calendar: true
    }

    constructor(private service: TreeService){}

    ngOnInit(){
        this.getCategory();
        this.getPlanting();
    }

    getCategory(){
      this.service.getCategory().subscribe(
          data => this.categorys = data
      )
    }

    getTree(category: Category){
      this.selectedCategory = category;
      this.changeView(2);
      this.service.getTreeByCategory(this.selectedCategory.id).subscribe(
          data => this.trees = data
      )
    }

    getMinTree(cat:Category){
      this.selectedCategory = cat;
      this.changeView(3);
      this.service.getMinCalendaryByCategory(this.selectedCategory.id).subscribe(
        data => {
          this.minTree = data;
        }
      )
    }

    getCalendary(tree: Tree){
      this.selectedTree = tree;
      this.changeView(4);
      this.service.getCalendaryTree(this.selectedTree.id).subscribe(
        data => this.calendars = data
      )
    }

    getPlanting(){
      this.service.getPlanting().subscribe(
        data => this.plantings = data
      )
    }

    onSubmit(){
      if (this.createXorUpdate.tree) {
        this.form.category = this.selectedCategory;
        this.service.postTree(this.form).subscribe(
          data => this.handleResponseTree(data, true)
        )
      } else {
        this.service.updateTree(this.form).subscribe(
          data => this.handleResponseTree(data, false)
        )
      }
      this.createXorUpdate.tree = true;
    }

    handleResponseTree(data:any, saveMode: boolean){
        if(data.success){
          let tree: Tree = data.tree;
          if (saveMode) {
            this.trees.push(tree);
          } else {
            const idx = this.trees.findIndex(item => item.id == tree.id);
            this.trees[idx] = tree;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
          console.log("Some thimg is wrong");
        }
    }

    onSubmitCalendar(f:NgForm){
      if (this.createXorUpdate.calendar) {
        this.formCalendar.tree = this.selectedTree;
        this.service.postCalendaryTree(this.formCalendar).subscribe(
          data => {
            this.calendars.push(data);
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }else{
        this.service.updateCalendar(this.formCalendar).subscribe(
          data => {
            let calendar: CalendaryTree = data['calendar'];
            const idx = this.calendars.findIndex(item => item.id == calendar.id);
            this.calendars[idx] = calendar;
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }
      this.createXorUpdate.calendar = true;
    }

    onSubmitCategory(){
      if (this.createXorUpdate.category) {
        this.service.postCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, true)
        )
      } else {
        this.service.updateCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, false)
        )
      }
      this.createXorUpdate.category = true;
    }

    handleResponseCategory(data:any, saveMode: boolean){
      if(data.success){
        let category: Category = data.category;
        if (saveMode) {
          this.categorys.push(category);
        } else {
          const idx = this.categorys.findIndex(item => item.id == category.id);
          this.categorys[idx] = category;
        }
        this.resetFormCategory();
        this.activeFormTemplate(false);
      }else{
          this.error = "Some thimg is wrong";
          setTimeout(() => { this.error = ""; }, 2000);
          console.log(data);
      }
    }

    onSubmitMin(){
      if (this.createXorUpdate.minCalendar) {
        this.formMin.category = this.selectedCategory;
        this.service.postMin(this.formMin).subscribe(
          data => {
            this.minTree.push(data);
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      } else {
        this.service.updateMinCalendar(this.formMin).subscribe(
          data => {
            let minCal: MinTree = data['minCalendar'];
            const idx = this.minTree.findIndex(item => item.id == minCal.id);
            this.minTree[idx] = minCal;
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      }
      this.createXorUpdate.minCalendar = true;
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteTree(id).subscribe(
            data => {
              this.trees = this.trees.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteCategory(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCategory(id).subscribe(
            data => {
              this.categorys = this.categorys.filter(item => item.id != id);
              Swal.fire(
                'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCalendar(id).subscribe(
            data => {
              console.log(data);
              this.calendars = this.calendars.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteMinCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteMinCalendar(id).subscribe(
            data => {
              console.log(data);
              this.minTree = this.minTree.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingCategory(category: Category){
      this.formTemplate = true;
      this.editFormCategory(category);
      this.createXorUpdate.category = false;
      window.scroll(0, 0);
    }

    editingTree(tree: Tree){
      this.formTemplate = true;
      this.editFormTree(tree);
      this.createXorUpdate.tree = false;
      window.scroll(0, 0);
    }

    editingCalendar(calendar: CalendaryTree){
      this.formTemplate = true;
      this.editFormCalendar(calendar);
      this.createXorUpdate.calendar = false;
      window.scroll(0, 0);
    }

    editingMinCalendar(minTree: MinTree){
      this.formTemplate = true;
      this.editFormMin(minTree);
      this.createXorUpdate.minCalendar = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2 || this.currentView === 3) {
        this.changeView(1);
      } else if (this.currentView === 4) {
        this.changeView(2);
      }
    }

    cancelCategory(){
      this.activeFormTemplate(false);
      this.resetFormCategory();
      this.createXorUpdate.category = true;
    }

    cancelTree(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate.tree = true;
    }

    cancelCalendar(){
      this.activeFormTemplate(false);
      this.resetFormCalendar();
      this.createXorUpdate.calendar = true;
    }

    cancelMinCalendar(){
      this.activeFormTemplate(false);
      this.resetFormMin();
      this.createXorUpdate.minCalendar = true;
    }

    resetForm(){
      this.form = new Tree();
    }

    resetFormCalendar(){
      this.formCalendar = new CalendaryTree();
    }

    resetFormCategory(){
      this.formCategory = new Category();
    }

    resetFormMin(){
      this.formMin = new MinTree();
    }

    editFormCategory(category: Category){
      this.formCategory.edit(category);
    }

    editFormTree(tree: Tree){
      tree.category = this.selectedCategory;
      tree.planting = this.plantings.filter(item => item.name === tree.plantingName)[0];
      this.form.edit(tree);
    }

    editFormCalendar(calendar: CalendaryTree){
      this.formCalendar.edit(calendar);
      this.formCalendar.tree = this.selectedTree;
    }

    editFormMin(minTree: MinTree){
      minTree.category = this.selectedCategory;
      this.formMin.edit(minTree);
    }

}