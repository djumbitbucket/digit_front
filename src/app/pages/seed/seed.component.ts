import { Component, OnInit } from "@angular/core";
import { Seed } from "src/commons/seed";
import { Speculation } from "src/commons/speculation";
import { SeedService } from "src/services/seed.service";

@Component({
    selector: 'app-seed',
    templateUrl: 'seed.component.html'
})

export class SeedComponent implements OnInit{

    public seeds:Seed[];
    public speculations: Speculation[];
    public selectedSeed: Seed;
    public form: Seed = new Seed();

    constructor(private service: SeedService){}

    ngOnInit(){
        this.getSeed();
    }

    getSeed(){
        this.service.getSeed().subscribe(
            data => this.seeds = data
        )
    }

    onSubmit(){
        this.service.postSeed(this.form).subscribe(
            data => {
                this.seeds.push(data);
                this.resetForm();
            }
        )
    }

    getSpeculation(seed: Seed){
        this.selectedSeed = seed;
        this.service.getSpeculation(this.selectedSeed.id).subscribe(
            data => this.speculations = data
        )
    }

    resetForm(){
        this.form.seedName = null;
    }
}