import { Component } from "@angular/core";

@Component({
    selector: 'app-homeFarming',
    templateUrl: 'homeFarming.component.html'
})

export class HomeFarming{
    public viewFarming = "x";

    onChangeView(val:string){
        this.viewFarming = val;
    }
}