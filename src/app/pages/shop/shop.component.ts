import { Component, OnInit } from '@angular/core';
import { Cashier } from 'src/commons/Cashier';
import { Shop } from 'src/commons/shop';
import { User } from 'src/commons/user';
import { ShopService } from 'src/services/shop.service';
import { UserService } from 'src/services/user.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  selectedShop: Shop;
  p:number = 1;
  page:number = 1;
  public currentView: number = 1; //{shop: 1, user: 2}
  formTemplate: boolean = false;
  
  public form: Shop = new Shop();
  public shops: Shop[];
  public users: User[]; 
  public shopUsers: User[] = []; //caissiers de la boutique
  public availableUsers: User[];
  public cashier = new Cashier();

  onAddUser: boolean = false;

  public error: string = "";

  public createXorUpdate: boolean = true;

  constructor(private service: ShopService, private service2: UserService){}
    ngOnInit(): void {
      this.getData();
      this.getAllUsersCashiers();
    }

    getData(){
      this.service.getShop().subscribe(
        data => this.shops = data
      )
    }

    getAllUsersCashiers(){
      this.service2.getUser().subscribe(
        data => {
          this.users = data.filter(u => u.role.roleName === "cashier"); 
          //console.log('all users ', this.users)
        }
      )
    }

    getShop(shop: Shop){
      this.selectedShop = shop;
      this.changeView(2);
      this.service.getUsersByShop(this.selectedShop.id).subscribe(
        data => {
          this.shopUsers = data;
          //console.log('current shopUsers', this.shopUsers);
        }
      );
    }

    addUser(shop: Shop){
      this.formTemplate = false;
      //this.onAddUser = true;
      this.cashier.shop = shop;
      window.scroll(0, 0);
      this.service.getUsersByShop(shop.id).subscribe(
        data => {
          this.setUsersAvailable(data);
          this.onAddUser = true;
        }
      );
    }

    CancelAddUser(){
      this.onAddUser = false;
    }

    onSelectUser(user: User){
      this.cashier.user = user;
      this.service.postCashier(this.cashier).subscribe(
        (data: any) => {
          this.onAddUser = false;
          if (data.success) {
            Swal.fire('Shop user!', "L'utilisateur est affecté avec succès!", 'success');
          } else {
            Swal.fire('Shop user!', data.message, 'error')
          }
        }
      );
    }

    setUsersAvailable(data: User[]){      
      this.availableUsers = this.users.filter(u => !data.find(v => v.id === u.id));
      //console.log('availableUsers', this.availableUsers);
    }

    onSubmit(){
      if (this.createXorUpdate) {
        this.service.postShop(this.form).subscribe(
          data => this.handleResponseShop(data, true)
        )
      } else {
        this.service.updateShop(this.form).subscribe(
          data => this.handleResponseShop(data, false)
        )
      }
      this.createXorUpdate = true;
    }

    handleResponseShop(data:any, saveMode: boolean){
        if(data.success){
          let shop: Shop = data.shop;
          if (saveMode) {
            this.shops.push(shop);
          } else {
            const idx = this.shops.findIndex(item => item.id == shop.id);
            this.shops[idx] = shop;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteShop(id).subscribe(
            data => {
              this.shops = this.shops.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingShop(shop: Shop){
      this.formTemplate = true;
      this.editForm(shop);
      this.createXorUpdate = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2) {
        this.changeView(1);
      }
    }

    cancelShop(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate = true;
    }

    resetForm(){
      this.form = new Shop();
    }

    editForm(shop: Shop){
      this.form.edit(shop);
    }

}
