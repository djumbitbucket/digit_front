import { Component, OnInit } from "@angular/core";
import { Bowl } from "src/commons/bowl";
import { CalendaryFish } from "src/commons/calendaryFish";
import { Category } from "src/commons/category";
import { Fish } from "src/commons/fish";
import { MinFish } from "src/commons/minFish";
import { FishService } from "src/services/fish.service";
import Swal from "sweetalert2";

@Component({
    selector: 'app-fish',
    templateUrl: 'fish.component.html'
})

export class FishComponent implements OnInit{

public bowls: Bowl[];
public fishs: Fish[];
public categorys: Category[];
public calendars: CalendaryFish[];
public minFish: MinFish[];

public form: Fish = new Fish();

public formCategory: Category = new Category();

public formCalendary: CalendaryFish = new CalendaryFish();

public formMin: MinFish = new MinFish();

public selectedFish: Fish = null;
public selectedCategory: Category = null;
p:number = 1;
page:number = 1;
formTemplate: boolean = false;
public currentView: number = 1; //{ctegory: 1, fish: 2, minFish: 3, calendar: 4}

public error: string = "";

public createXorUpdate: any = {
  category: true,
  fish: true,
  minCalendar: true,
  calendar: true
}

constructor(private service: FishService){}

    ngOnInit(){
        this.getCategory();
        this.getBowl();
    }

    clearError(){
        console.log('clear error');
        this.error = "";
    }

    onSubmit(){
      if (this.createXorUpdate.fish) {
        this.form.category = this.selectedCategory;
        this.service.postFish(this.form).subscribe(
          data => this.handleResponseFish(data, true)
        )
      } else {
        this.service.updateFish(this.form).subscribe(
          data => this.handleResponseFish(data, false)
        )
      }
      this.createXorUpdate.fish = true;
    }

    handleResponseFish(data:any, saveMode: boolean){
      if(data.success){
        let fish: Fish = data.fish;
        if (saveMode) {
          this.fishs.push(fish);
        } else {
          const idx = this.fishs.findIndex(item => item.id == fish.id);
          this.fishs[idx] = fish;
        }
        this.resetForm();
        this.activeFormTemplate(false);
      }else{
        console.log("Some thimg is wrong");
      }
    }

    onSubmitCategory(){
      if (this.createXorUpdate.category) {
        this.service.postCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, true)
        )
      } else {
        this.service.updateCategory(this.formCategory).subscribe(
          data => this.handleResponseCategory(data, false)
        )
      }
      this.createXorUpdate.category = true;
    }

    handleResponseCategory(data:any, saveMode: boolean){
        if(data.success){
          let category: Category = data.category;
          if (saveMode) {
            this.categorys.push(category);
          } else {
            const idx = this.categorys.findIndex(item => item.id == category.id);
            this.categorys[idx] = category;
          }
          this.resetFormCategory();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    onSubmitCalendary(){
      if (this.createXorUpdate.calendar) {
        this.formCalendary.fish = this.selectedFish;
        this.service.postCalendary(this.formCalendary).subscribe(
          data => {
            this.calendars.push(data);
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }else{
        this.service.updateCalendar(this.formCalendary).subscribe(
          data => {
            let calendar: CalendaryFish = data['calendar'];
            const idx = this.calendars.findIndex(item => item.id == calendar.id);
            this.calendars[idx] = calendar;
            this.activeFormTemplate(false);
            this.resetFormCalendar();
          }
        )
      }
      this.createXorUpdate.calendar = true;
    }

    onSubmitMin(){
      if (this.createXorUpdate.minCalendar) {
        this.formMin.category = this.selectedCategory;
        this.service.postMin(this.formMin).subscribe(
          data => {
            this.minFish.push(data);
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      } else {
        this.service.updateMinCalendar(this.formMin).subscribe(
          data => {
            let minCal: MinFish = data['minCalendar'];
            const idx = this.minFish.findIndex(item => item.id == minCal.id);
            this.minFish[idx] = minCal;
            this.activeFormTemplate(false);
            this.resetFormMin();
          }
        )
      }
      this.createXorUpdate.minCalendar = true;
    }

    getCategory(){
        this.service.getCategory().subscribe(
            data => this.categorys = data
        )
    }

    getBowl(){
        this.service.getBowl().subscribe(
            data => this.bowls = data
        )
    }

    getFish(cat: Category){
      this.selectedCategory = cat;
      this.changeView(2);
      this.service.getFishByCategory(this.selectedCategory.id).subscribe(
        data => this.fishs = data
      )
    }

    getCalendary(fish: Fish){
      this.selectedFish = fish;
      this.changeView(4);
      this.service.getCalendaryFish(this.selectedFish.id).subscribe(
        data => this.calendars = data
      )
    }

    getMinFish(cat: Category){
      this.selectedCategory = cat;
      this.changeView(3);
      this.service.getMinCalendaryByCategory(this.selectedCategory.id).subscribe(
        data => {
          this.minFish = data;
        }
      )
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteFish(id).subscribe(
            data => {
              this.fishs = this.fishs.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteCategory(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCategory(id).subscribe(
            data => {
              this.categorys = this.categorys.filter(item => item.id != id);
              Swal.fire(
                'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteCalendar(id).subscribe(
            data => {
              console.log(data);
              this.calendars = this.calendars.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    onDeleteMinCalendar(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteMinCalendar(id).subscribe(
            data => {
              console.log(data);
              this.minFish = this.minFish.filter(item => item.id != id);
              Swal.fire( 'Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingCategory(category: Category){
      this.formTemplate = true;
      this.editFormCategory(category);
      this.createXorUpdate.category = false;
      window.scroll(0, 0);
    }

    editingFish(fish: Fish){
      this.formTemplate = true;
      this.editFormFish(fish);
      this.createXorUpdate.fish = false;
      window.scroll(0, 0);
    }

    editingCalendar(calendar: CalendaryFish){
      this.formTemplate = true;
      this.editFormCalendar(calendar);
      this.createXorUpdate.calendar = false;
      window.scroll(0, 0);
    }

    editingMinCalendar(minFish: MinFish){
      this.formTemplate = true;
      this.editFormMin(minFish);
      this.createXorUpdate.minCalendar = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2 || this.currentView === 3) {
        this.changeView(1);
      } else if (this.currentView === 4) {
        this.changeView(2);
      }
    }

    cancelCategory(){
      this.activeFormTemplate(false);
      this.resetFormCategory();
      this.createXorUpdate.category = true;
    }

    cancelFish(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate.fish = true;
    }

    cancelCalendar(){
      this.activeFormTemplate(false);
      this.resetFormCalendar();
      this.createXorUpdate.calendar = true;
    }

    cancelMinCalendar(){
      this.activeFormTemplate(false);
      this.resetFormMin();
      this.createXorUpdate.minCalendar = true;
    }

    resetForm(){
      this.form = new Fish();
    }

    resetFormCalendar(){
      this.formCalendary = new CalendaryFish();
    }

    resetFormCategory(){
      this.formCategory = new Category();
    }

    resetFormMin(){
      this.formMin = new MinFish();
    }

    editFormCategory(category: Category){
      this.formCategory.edit(category);
    }

    editFormFish(fish: Fish){
      fish.category = this.selectedCategory;
      fish.bowl = this.bowls.filter(item => item.name === fish.bowlName)[0];
      this.form.edit(fish);
    }

    editFormCalendar(calendar: CalendaryFish){
      calendar.fish = this.selectedFish;
      this.formCalendary.edit(calendar);
    }

    editFormMin(minFish: MinFish){
      minFish.category = this.selectedCategory;
      this.formMin.edit(minFish);
    }

}