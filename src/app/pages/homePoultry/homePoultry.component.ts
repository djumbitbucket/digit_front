import { Component } from "@angular/core";

@Component({
    selector: 'app-homePoultry',
    templateUrl: 'homePoultry.component.html'
})

export class HomePoultryComponent{
    public viewPoultry = true;

    onChangeView(val:boolean){
        this.viewPoultry = val;
    }
}