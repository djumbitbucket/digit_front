import { Component, OnInit } from "@angular/core";
import { Cattle } from "src/commons/cattle";
import { Enclosure } from "src/commons/enclosure";
import { EnclosureService } from "src/services/enclosure.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-enclosure",
  templateUrl: "enclosure.component.html"
})
export class EnclosureComponent implements OnInit {

  selectedEnclosure:Enclosure;
  p:number = 1;
  page:number = 1;
  public currentView: number = 1; //{enclosure: 1, cattle: 2}
  formTemplate: boolean = false;
  
  public form: Enclosure = new Enclosure();
  public enclosures: Enclosure[];
  public cattles: Cattle[];

  public error: string = "";

  public createXorUpdate: boolean = true;

  constructor(private service:EnclosureService){}
    ngOnInit(): void {
        this.getData();
    }

    onSubmit(){
      if (this.createXorUpdate) {
        this.service.postEnclosure(this.form).subscribe(
          data => this.handleResponseEnclosure(data, true)
        )
      } else {
        this.service.updateEnclosure(this.form).subscribe(
          data => this.handleResponseEnclosure(data, false)
        )
      }
      this.createXorUpdate = true;
    }

    handleResponseEnclosure(data:any, saveMode: boolean){
        if(data.success){
          let enclosure: Enclosure = data.enclosure;
          if (saveMode) {
            this.enclosures.push(enclosure);
          } else {
            const idx = this.enclosures.findIndex(item => item.id == enclosure.id);
            this.enclosures[idx] = enclosure;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    getData(){
      this.service.getEnclosure().subscribe(
        data => this.enclosures = data
      )
    }

    getEnclosure(enclosure:Enclosure){
      this.selectedEnclosure = enclosure;
      this.changeView(2);
      this.service.getCattle(this.selectedEnclosure.id).subscribe(
        data => this.cattles = data
      )
    }

    onDelete(id:number){
      Swal.fire({
        title: 'Are you sure?',
        text: "Voulez vous supprimer cet enregistrement?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.deleteEnclosure(id).subscribe(
            data => {
              this.enclosures = this.enclosures.filter(item => item.id != id);
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
            }
          )
        }
      })
    }

    editingEnclosure(enclosure: Enclosure){
      this.formTemplate = true;
      this.editForm(enclosure);
      this.createXorUpdate = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2) {
        this.changeView(1);
      }
    }

    cancelEnclosure(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate = true;
    }

    resetForm(){
      this.form = new Enclosure();
    }

    editForm(enclosure: Enclosure){
      this.form.edit(enclosure);
    }

}