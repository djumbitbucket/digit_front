import { Component, OnInit } from '@angular/core';
import { Role } from 'src/commons/role';
import { User } from 'src/commons/user';
import { UserService } from 'src/services/user.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  p:number = 1;
  page:number = 1;
  public currentView: number = 1; //{user: 1}
  formTemplate: boolean = false;
  
  public form: User = new User();
  public users: User[];
  selectedUser: User;
  public roles: Role[];
  
  public error: string = "";

  public createXorUpdate: boolean = true;

  constructor(private service: UserService){}

    ngOnInit(): void {
        this.getData();
        this.getRoles();
    }

    getData(){
      this.service.getUser().subscribe(
        data => this.users = data
      )
    }

    getRoles(){
      this.service.getRoles().subscribe(
        data => this.roles = data
      )
    }

    roleSelected(){
      this.form.role = this.form.role_id == 0 ? null : this.roles.find(r => r.id === this.form.role_id);
      //console.log('on roleChanged, user = ', this.form)
    }

    onSubmit(){
      if (this.createXorUpdate) {
        this.service.postUser(this.form).subscribe(
          data => this.handleResponseUser(data, true)
        )
      } else {
        this.service.updateUser(this.form).subscribe(
          data => this.handleResponseUser(data, false)
        )
      }
      this.createXorUpdate = true;
    }

    handleResponseUser(data:any, saveMode: boolean){
        if(data.success){
          let user: User = data.user;
          if (saveMode) {
            this.users.push(user);
          } else {
            const idx = this.users.findIndex(item => item.id == user.id);
            this.users[idx] = user;
          }
          this.resetForm();
          this.activeFormTemplate(false);
        }else{
            this.error = "Some thimg is wrong";
            setTimeout(() => { this.error = ""; }, 2000);
            console.log(data);
        }
    }

    canSeTPwd(){
      return !this.createXorUpdate;
    }

    onActivation(u: User){
      Swal.fire({
        title: 'Are you sure?',
        text: `Voulez vous ${u.active ? "désactiver": "activer"} cet utilisateur?`,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: `Yes, ${u.active ? 'deactivate' : 'activate'}!`
      }).then((result) => {
        if (result.isConfirmed) {
          this.service.activation(u.id).subscribe(
            (data: any) => {          
              if (data.success) {
                let user: User = data.user;
                const idx = this.users.findIndex(u => u.id === user.id)
                this.users[idx] = user;
                Swal.fire('Activation success!', `Utilisateur ${user.active ? 'activé' : 'désactivé'} avec succès!`, 'success')
              } else {
                Swal.fire('Activation fail!', data.message, 'error')
              }
            }
          );
        }
      })
    }

    editingUser(user: User){
      this.formTemplate = true;
      this.editForm(user);
      this.createXorUpdate = false;
      window.scroll(0, 0);
    }

    changeView(viewNumber: number){
      this.currentView = viewNumber;
    }
  
    activeFormTemplate(value: boolean){
      this.formTemplate = value;
    }
    
    goBack(){
      if (this.currentView === 2) {
        this.changeView(1);
      }
    }

    cancelUser(){
      this.activeFormTemplate(false);
      this.resetForm();
      this.createXorUpdate = true;
    }

    resetForm(){
      this.form = new User();
    }

    editForm(user: User){
      this.form.edit(user);
    }

}
