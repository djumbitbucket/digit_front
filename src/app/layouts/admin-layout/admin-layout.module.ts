import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { UserComponent } from "../../pages/user/user.component";
import { CattleComponent } from "src/app/pages/cattle/cattle.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { EnclosureComponent } from "src/app/pages/enclosure/enclosure.component";
import { ChickenCoopComponent } from "src/app/pages/chickenCoop/chickenCoop.component";
import { PoultryComponent } from "src/app/pages/poultry/poultry.component";
import {NgxPaginationModule} from 'ngx-pagination'; 
import { BowlComponent } from "src/app/pages/bowl/bowl.component";
import { FishComponent } from "src/app/pages/fish/fish.component";
import { SpeculationComponent } from "src/app/pages/speculation/speculation.component";
import { PlantingComponent } from "src/app/pages/planting/planting.component";
import { TreeComponent } from "src/app/pages/tree/tree.component";
import { HomeCattleComponent } from "src/app/pages/homeCattle/homeCattle.component";
import { HomeFishComponent } from "src/app/pages/homeFish/homeFish.component";
import { HomePoultryComponent } from "src/app/pages/homePoultry/homePoultry.component";
import { HomeFarming } from "src/app/pages/homeFarming/homeFarming.component";
import { HomeCommerceComponent } from 'src/app/pages/home-commerce/home-commerce.component';
import { StockProduitComponent } from 'src/app/pages/stock-produit/stock-produit.component';
import { StockMatiereComponent } from 'src/app/pages/stock-matiere/stock-matiere.component';
import { VenteComponent } from 'src/app/pages/vente/vente.component';
import { HomeUsersComponent } from 'src/app/pages/home-users/home-users.component';
import { ShopComponent } from 'src/app/pages/shop/shop.component';
import { ProfileComponent } from 'src/app/pages/profile/profile.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgxPaginationModule
  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    CattleComponent,
    ChickenCoopComponent,
    EnclosureComponent,
    PoultryComponent,
    SpeculationComponent,
    BowlComponent,
    FishComponent,
    PlantingComponent,
    TreeComponent,
    HomeCattleComponent,
    HomeFishComponent,
    HomePoultryComponent,
    HomeFarming,
    HomeCommerceComponent, 
    StockProduitComponent, 
    StockMatiereComponent, 
    VenteComponent, 
    HomeUsersComponent, 
    ShopComponent, 
    ProfileComponent
  ]
})
export class AdminLayoutModule {}
