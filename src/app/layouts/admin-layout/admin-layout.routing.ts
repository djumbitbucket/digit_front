import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { UserComponent } from "../../pages/user/user.component";
import { CattleComponent } from "src/app/pages/cattle/cattle.component";
import { EnclosureComponent } from "src/app/pages/enclosure/enclosure.component";
import { ChickenCoopComponent } from "src/app/pages/chickenCoop/chickenCoop.component";
import { PoultryComponent } from "src/app/pages/poultry/poultry.component";
import { BowlComponent } from "src/app/pages/bowl/bowl.component";
import { FishComponent } from "src/app/pages/fish/fish.component";
import { SpeculationComponent } from "src/app/pages/speculation/speculation.component";
import { PlantingComponent } from "src/app/pages/planting/planting.component";
import { TreeComponent } from "src/app/pages/tree/tree.component";
import { HomeCattleComponent } from "src/app/pages/homeCattle/homeCattle.component";
import { HomeFishComponent } from "src/app/pages/homeFish/homeFish.component";
import { HomePoultryComponent } from "src/app/pages/homePoultry/homePoultry.component";
import { HomeFarming } from "src/app/pages/homeFarming/homeFarming.component";
import { StockProduitComponent } from "src/app/pages/stock-produit/stock-produit.component";
import { StockMatiereComponent } from "src/app/pages/stock-matiere/stock-matiere.component";
import { VenteComponent } from "src/app/pages/vente/vente.component";
import { HomeCommerceComponent } from "src/app/pages/home-commerce/home-commerce.component";
import { HomeUsersComponent } from 'src/app/pages/home-users/home-users.component';

export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "cattle", component: CattleComponent},
  { path: "enclosure", component: EnclosureComponent},
  { path: "chickenCoop", component: ChickenCoopComponent},
  { path: "poultry", component: PoultryComponent},
  { path: "speculation", component: SpeculationComponent},
  { path: "user", component: UserComponent },
  { path: "bowl", component: BowlComponent},
  { path: "fish", component: FishComponent},
  { path: "planting", component: PlantingComponent},
  { path: "tree", component: TreeComponent},
  { path: "stock-produit", component: StockProduitComponent},
  { path: "stock-matiere", component: StockMatiereComponent},
  { path: "vente", component: VenteComponent},
  { path: "home/cattle", component: HomeCattleComponent},
  { path: "home/fish", component: HomeFishComponent},
  { path: "home/poultry", component: HomePoultryComponent},
  { path: "home/farming", component: HomeFarming},
  { path: "home/commerce", component: HomeCommerceComponent},
  { path: "home/rh", component: HomeUsersComponent},
];
