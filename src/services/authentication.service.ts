import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LoggerValue } from '../commons/loggerValue';
import { environment } from 'src/environments/environment';

export class User{
  constructor(
    public status:string,
     ) {}
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  private baseUrl = environment.apiOut;
  private authUrl = environment.apiAuth;

  constructor(private httpClient:HttpClient) {}


     authenticate(username: string, password: string): Observable<LoggerValue> {
      const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) }); 
      //192.168.31.80
      return this.httpClient.post<GetResponseLogger>(this.authUrl,{"username" :username ,"password" : password}).pipe(
      map(
          response => response.data
      ))
     }

  isUserLoggedIn() {
    let token = sessionStorage.getItem('token')
    console.log(!(token === null))
    return !(token === null)
  }

  logOut() {
    sessionStorage.removeItem('username')
  }
}

interface GetResponseLogger{
    data:LoggerValue
}