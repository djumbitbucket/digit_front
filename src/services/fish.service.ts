import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { Bowl } from "src/commons/bowl";
import { CalendaryFish } from "src/commons/calendaryFish";
import { Category } from "src/commons/category";
import { Fish } from "src/commons/fish";
import { MinFish } from "src/commons/minFish";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: "root"
})

export class FishService{

    private baseUrl = environment.apiOut;
    private baseUrlFish = "fish";
    private baseUrlCategory = "fishCategory";
    private baseUrlByCategory = "fishCategories";
    private baseUrlBowl = "bowls";
    private baseUrlCalendar = "add/calendar/fish";
    private postMinFish = "min/calendar/fish";

    constructor(private httpClient: HttpClient){}

    getData():Observable<Fish[]>{
        return this.httpClient.get<GetResponseFish>(this.baseUrl + this.baseUrlFish).pipe(
            map( response => response._embedded.fish)
        )
    }

    getFishByCategory(id:number):Observable<Fish[]>{
        return this.httpClient.get<GetResponseFish>(this.baseUrl + this.baseUrlByCategory+`/${id}/fish`).pipe(
            map( response => response._embedded.fish)
        )
    }

    getCategory():Observable<Category[]>{
        return this.httpClient.get<GetResponseCategory>(this.baseUrl + this.baseUrlCategory).pipe(
            map( response => response.categorys)
        )
    }

    getBowl():Observable<Bowl[]>{
        return this.httpClient.get<GetResponseBowl>(this.baseUrl + this.baseUrlBowl).pipe(
            map( response => response._embedded.bowls)
        )
    }

    getCalendaryFish(id:number):Observable<CalendaryFish[]>{
        return this.httpClient.get<GetResponseCalendary>(this.baseUrl + this.baseUrlFish+`s/${id}/calendary`).pipe(
            map( response => response._embedded.calendaryFish)
        )
    }

    getMinCalendaryByCategory(id:number):Observable<MinFish[]>{
        return this.httpClient.get<GetResponseMinFish>(this.baseUrl + this.baseUrlByCategory+`/${id}/minCalendary`).pipe(
            map( response => response._embedded.fishCalendaryMins)
        )
    }

    postCategory(data: Category){
        return this.httpClient.post(this.baseUrl + this.baseUrlCategory,data)
    }

    postFish(data:Fish){
        return this.httpClient.post(this.baseUrl + this.baseUrlFish,data)
    }

    postCalendary(data: CalendaryFish):Observable<CalendaryFish>{
        return this.httpClient.post<PostResponseCalendary>(this.baseUrl + this.baseUrlCalendar,data).pipe(
            map( response => response.calendar)
        )
    }

    postMin(data:MinFish):Observable<MinFish>{
        return this.httpClient.post<PostResponseMinFish>(this.baseUrl + this.postMinFish,data).pipe(
            map( response => response.minCalendar)
        )
    }
    
    deleteFish(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlFish+`/${id}`)
    }
    
    deleteCategory(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlCategory+`/${id}`)
    }
    
    deleteCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `delete/calendar/fish/${id}`)
    }
    
    deleteMinCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `min/calendar/fish/${id}`)
    }

    updateCategory(data: Category){
        return this.httpClient.put(this.baseUrl + this.baseUrlCategory, data)
    }

    updateFish(data: Fish){
        return this.httpClient.put(this.baseUrl + this.baseUrlFish, data)
    }

    updateCalendar(data: CalendaryFish){
        return this.httpClient.post(this.baseUrl + this.baseUrlCalendar, data)
    }

    updateMinCalendar(data: MinFish){
        return this.httpClient.post(this.baseUrl + this.postMinFish, data)
    }
}

interface GetResponseFish{
    _embedded:{
        fish: Fish[]
    }
}

interface PostResponseFish{
    fish: Fish
}

interface GetResponseCategory{
    categorys: Category[]
}

interface GetResponseBowl{
    _embedded:{
        bowls: Bowl[]
    }
}

interface GetResponseCalendary{
    _embedded:{
        calendaryFish: CalendaryFish[]
    }
}

interface PostResponseCalendary{
    calendar: CalendaryFish
}

interface PostResponseMinFish{
    minCalendar: MinFish
}

interface GetResponseMinFish{
    _embedded:{
        fishCalendaryMins:MinFish[]
    }
}