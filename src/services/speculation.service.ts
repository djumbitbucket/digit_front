import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Speculation } from "src/commons/speculation";
import { map, Observable } from "rxjs";
import { CalendarySpeculation } from "src/commons/calendarySpeculation";
import { Seed } from "src/commons/seed";
import { Planting } from "src/commons/planting";
import { MinSpeculation } from "src/commons/minSpeculation";
import { environment } from "src/environments/environment";
import { Harvest } from "src/commons/harvest";
import { CalendarFrequency } from "src/commons/CalendarFrequency";

@Injectable({
    providedIn: 'root'
})
export class SpeculationService{

    private baseUrl = environment.apiOut;
    private baseUrlFarming = "speculation";
    private baseUrlSeed = "seed";
    private baseUrlCalendary = "add/calendar/speculation";
    private baseUrlPlanting = "plantings";
    private postMinSpeculation = "min/calendar/speculation";
    private baseUrlHarvest = "harvest";
    private baseUrlCalendarFrequency = "frequence/calendar/speculation";

    
    constructor(private httpClient: HttpClient){}

    getData():Observable<Speculation[]>{
        return this.httpClient.get<GetResponseFarming>(this.baseUrl + this.baseUrlFarming+"s").pipe(
            map( responce => responce._embedded.speculations)
        )
    }

    getSeed():Observable<Seed[]>{
        return this.httpClient.get<GetResponseSeed>(this.baseUrl + this.baseUrlSeed).pipe(
            map( responce => responce.seeds)
        )
    }

    getMinCalendaryByCategory(id:number):Observable<MinSpeculation[]>{
        return this.httpClient.get<GetResponseMinSpeculation>(this.baseUrl + this.baseUrlSeed+`s/${id}/minCalendary`).pipe(
            map( response => response._embedded.speculationCalendaryMins)
        )
    }

    postMin(data:MinSpeculation):Observable<MinSpeculation>{
        return this.httpClient.post<PostResponseMinSpeculation>(this.baseUrl + this.postMinSpeculation,data).pipe(
            map( response => response.minCalendar)
        )
    }

    postSpeculation(data:Speculation){
        return this.httpClient.post(this.baseUrl + this.baseUrlFarming,data)
    }

    getCalendary(id:number):Observable<CalendarySpeculation[]>{
        return this.httpClient.get<GetResponseCalendary>(this.baseUrl + this.baseUrlFarming+`s/${id}/calendary`).pipe(
            map( response => response._embedded.calendarySpeculations)
        )
    }

    getSpeculationBySeed(id:number):Observable<Speculation[]>{
        return this.httpClient.get<GetResponseFarming>(this.baseUrl + this.baseUrlSeed+`s/${id}/speculation`).pipe(
            map( response => response._embedded.speculations)
        )
    }

    postCalendary(data: any):Observable<CalendarySpeculation>{
        return this.httpClient.post<PostResponseCalendary>(this.baseUrl + this.baseUrlCalendary,data).pipe(
            map( response => response.calendar)
        )
    }

    postSeed(data:Seed){
        return this.httpClient.post(this.baseUrl + this.baseUrlSeed,data)
    }

    getPlanting():Observable<Planting[]>{
        return this.httpClient.get<GetResponsePlanting>(this.baseUrl + this.baseUrlPlanting).pipe(
            map( response => response._embedded.plantings)
        )
    }

    deleteSpeculation(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlFarming+`/${id}`)
    }
    
    deleteSeed(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlSeed + "/"+id)
    }

    deleteCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `delete/calendar/speculation/${id}`)
    }

    deleteMinCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `min/calendar/speculation/${id}`)
    }

    updateSeed(data: Seed){
        return this.httpClient.put(this.baseUrl + this.baseUrlSeed, data)
    }

    updateSpeculation(data: Speculation){
        return this.httpClient.put(this.baseUrl + this.baseUrlFarming, data)
    }

    updateCalendar(data: CalendarySpeculation){
        return this.httpClient.post(this.baseUrl + this.baseUrlCalendary, data)
    }

    updateMinCalendar(data: MinSpeculation){
        return this.httpClient.post(this.baseUrl + this.postMinSpeculation, data)
    }
    
    //harvest
    getHarvest(id_speculation: number):Observable<Harvest[]>{
        return this.httpClient.get<GetResponseHarvest>(this.baseUrl + this.baseUrlHarvest +"/" + id_speculation).pipe(
            map ( responce => responce.harvests)
        )
    } 

    postHarvest(data: Harvest){
        return this.httpClient.post(this.baseUrl + this.baseUrlHarvest,data)
    }

    updateHarvest(data: Harvest){
        //console.log('updating harvest', data)
        return this.httpClient.put(this.baseUrl + this.baseUrlHarvest,data)
    }

    deleteHarvest(id: number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlHarvest + "/" + id)
    }

    //calendarFrequency
    postCalendary2(data: any){
        return this.httpClient.post(this.baseUrl + this.baseUrlCalendarFrequency,data)
    }

}

interface GetResponseSeed{
    seeds:Seed[];
}

interface GetResponseFarming{
    _embedded:{
        speculations: Speculation[];
    }
}

interface PostResponce{
    speculation : Speculation
}

interface GetResponseCalendary{
    _embedded:{
        calendarySpeculations:CalendarySpeculation[];
    }
}

interface GetResponseCalendary2{
    calendarySpeculations: CalendarFrequency[]
}

interface PostResponseCalendary{
    calendar: CalendarySpeculation;
}

interface GetResponsePlanting{
    _embedded:{
        plantings: Planting[];
    }
}

interface PostResponseMinSpeculation{
    minCalendar: MinSpeculation
}

interface GetResponseMinSpeculation{
    _embedded:{
        speculationCalendaryMins:MinSpeculation[]
    }
} 

interface GetResponseHarvest{
    harvests: Harvest[];
}