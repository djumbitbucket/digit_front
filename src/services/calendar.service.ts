import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { Calendar } from "src/commons/calendar";
import { environment } from "src/environments/environment";


@Injectable({
    providedIn: 'root'
})
export class CalendarService{

    private baseUrl = environment.apiOut;
    private baseUrlCalendary = "calendars";
    private baseUrlCalendaryCatlle = "add/calendar/cattle";
    private baseUrlCalendaryPoultry = "add/calendar/poultry";
    private baseUrlCalendaryFarming = "add/calendar/farming";

    constructor(private httpClient: HttpClient){}

    getData():Observable<Calendar[]>{
        return this.httpClient.get<GetResponseCalendary>(this.baseUrl + this.baseUrlCalendary).pipe(
            map( response => response._embedded.calendars)
        )
    }

    getCalendaryCattle(id:number):Observable<Calendar[]>{
        return this.httpClient.get<Calendar[]>(this.baseUrl + this.baseUrlCalendaryCatlle+"/"+id)
    }

    postCalendar(data:Calendar):Observable<Calendar>{
        return this.httpClient.post<Calendar>(this.baseUrl + this.baseUrlCalendary,data)
    }

    postCalendaryCattle(data):Observable<Calendar>{
        return this.httpClient.post<Calendar>(this.baseUrl + this.baseUrlCalendaryCatlle,data)

    }

    
}

interface GetResponseCalendary{
    _embedded:{
        calendars:[]
    }
}