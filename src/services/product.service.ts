import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Product } from "src/commons/product";
import { map } from "rxjs";
import { Speculation } from "src/commons/speculation";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class ProductService{
    
    private baseUrl = environment.apiOut;

    baseUrlProduct = "products";

    constructor(private httpClient:HttpClient){}

    getData():Observable<Product[]>{
        return this.httpClient.get<GetResponceProduct>(this.baseUrl + this.baseUrlProduct).pipe(
            map( response => response._embedded.products)
        )
    }

    postProduct(data:Product):Observable<Product>{
        return this.httpClient.post<Product>(this.baseUrl + this.baseUrlProduct,data)
    }

    getFarming(id:number):Observable<Speculation[]>{
        return this.httpClient.get<GetResponceFarming>(this.baseUrl + this.baseUrlProduct+`/${id}/farming`).pipe(
            map( response => response._embedded.farmings)
        )
    }
}

interface GetResponceProduct{
    _embedded:{
        products:Product[]
    }
}

interface GetResponceFarming{
    _embedded:{
        farmings:Speculation[]
    }
}