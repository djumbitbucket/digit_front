import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Cattle } from "src/commons/cattle";
import {map} from 'rxjs/operators';
import { Enclosure } from "src/commons/enclosure";
import { CalendaryCattle } from "src/commons/calendaryCattle";
import { Category } from "src/commons/category";
import { CategoryCattle } from "src/commons/categoryCattle";
import { MinCattle } from "src/commons/minCattle";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class CattleService{

    private baseUrl = environment.apiOut;
    private baseUrlCattle = "cattle";
    private baseUrlEnclosure = "enclosures";
    private baseUrlCategory = "cattleCategory";
    private baseUrlByCategory = "cattleCategories";
    private postCalendaryCatlle = "add/calendar/cattle";
    private postMinCatlle = "min/calendar/cattle";

    constructor(private httpClient:HttpClient){}

    getByName(name: string){
        return this.httpClient.get(this.baseUrl + this.baseUrlCattle+`/byName/?name=${name}`)
    }
    getCattle():Observable<Cattle[]>{
        return this.httpClient.get<GetResponseCattle>(this.baseUrl + this.baseUrlCattle+"s").pipe(
            map(
                response => response._embedded.cattles
            )
        )
    }

    getEnclosure():Observable<Enclosure[]>{
        return this.httpClient.get<GetResponseEnclosure>(this.baseUrl + this.baseUrlEnclosure).pipe(
            map(
                response => response._embedded.enclosures
            )
        )
    }

    getCalendaryCattle(id:number):Observable<CalendaryCattle[]>{
        return this.httpClient.get<GetResponseCalendary>(this.baseUrl + this.baseUrlCattle+`s/${id}/calendary`).pipe(
            map( responce => responce._embedded.calendaryCattles)
        )
    }

    getCategory():Observable<CategoryCattle[]>{
        return this.httpClient.get<GetResponseCategory>(this.baseUrl + this.baseUrlCategory).pipe(
            map(response => response.categorys)
        )
    }

    getCattleByCategory(id:number):Observable<Cattle[]>{
        return this.httpClient.get<GetResponseCattle>(this.baseUrl + this.baseUrlByCategory+`/${id}/cattle`).pipe(
            map( response => response._embedded.cattles)
        )
    }

    getMinCalendaryByCategory(id:number):Observable<MinCattle[]>{
        return this.httpClient.get<GetResponseMinCattle>(this.baseUrl + this.baseUrlByCategory+`/${id}/minCalendary`).pipe(
            map( response => response._embedded.cattleCalendaryMins)
        )
    }

    postCattle(data:Cattle){
        return this.httpClient.post(this.baseUrl + this.baseUrlCattle,data)
    }

    postCalendaryCattle(data):Observable<CalendaryCattle>{
        return this.httpClient.post<PostResponseCalendary>(this.baseUrl + this.postCalendaryCatlle,data).pipe(
            map( response => response.calendar)
        )

    }

    postCategory(data:CategoryCattle){
        return this.httpClient.post(this.baseUrl + this.baseUrlCategory, data)
    }

    postMinCattle(data:MinCattle):Observable<MinCattle>{
        return this.httpClient.post<PostResponseMinCattle>(this.baseUrl + this.postMinCatlle,data).pipe(
            map( response => response.minCalendar)
        )
    }

    deleteCattle(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlCattle+"/"+id)
    }

    deleteCategory(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlCategory+"/"+id)
    }

    deleteCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `delete/calendar/cattle/${id}`)
    }

    deleteMinCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + this.postMinCatlle + `/${id}`)
    }

    countCattle(){
        return this.httpClient.get(this.baseUrl + this.baseUrlCattle+"/count")
    }

    updateCategory(data: CategoryCattle){
        return this.httpClient.put(this.baseUrl + this.baseUrlCategory, data)
    }

    updateCattle(data: Cattle){
        return this.httpClient.put(this.baseUrl + this.baseUrlCattle, data)
    }

    updateCalendar(data: any){
        return this.httpClient.post(this.baseUrl + this.postCalendaryCatlle, data)
    }

    updateMinCalendar(data: MinCattle){
        return this.httpClient.post(this.baseUrl + this.postMinCatlle, data)
    }


}

interface GetResponseCattle{
    _embedded:{
        cattles:Cattle[]
    }
}

interface GetResponseEnclosure{
    _embedded:{
        enclosures:Enclosure[]
    }
}

interface GetResponseCalendary{
    _embedded:{
        calendaryCattles:CalendaryCattle[]
    }
}

interface PostResponseCalendary{
    calendar: CalendaryCattle
}

interface postCattleResponse{
    cattle:Cattle
}

interface GetResponseCategory{
    categorys: CategoryCattle[]
}

interface PostResponseMinCattle{
    minCalendar: MinCattle
}

interface GetResponseMinCattle{
    _embedded:{
        cattleCalendaryMins:MinCattle[]
    }
}