import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { CalendaryTree } from "src/commons/calendaryTree";
import { Category } from "src/commons/category";
import { MinTree } from "src/commons/minTree";
import { Planting } from "src/commons/planting";
import { Tree } from "src/commons/tree";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class TreeService{

    private baseUrl = environment.apiOut;
    private baseUrlTree = "tree";
    private baseUrlCategory = "treeCategory";
    private baseUrlByCategory = "treeCategories";
    private baseUrlPlanting = "plantings";
    private postMinTree = "min/calendar/tree";
    private baseUrlCalendaryTree = "add/calendar/tree";
    
    constructor(private httpClient: HttpClient){}

    getCategory():Observable<Category[]>{
        return this.httpClient.get<GetResponseCategory>(this.baseUrl + this.baseUrlCategory).pipe(
            map( response => response.categorys)
        )
    }

    getTreeByCategory(id:number):Observable<Tree[]>{
        return this.httpClient.get<GetResponseTree>(this.baseUrl + this.baseUrlByCategory+`/${id}/tree`).pipe(
            map( response => response._embedded.trees)
        )
    }

    postCategory(data:Category){
        return this.httpClient.post(this.baseUrl + this.baseUrlCategory,data)
    }

    postTree(data:Tree){
        return this.httpClient.post(this.baseUrl + this.baseUrlTree,data)
    }

    postCalendaryTree(data):Observable<CalendaryTree>{
        return this.httpClient.post<PostResponseCalendary>(this.baseUrl + this.baseUrlCalendaryTree,data).pipe(
            map( response => response.calendar)
        )

    }

    getPlanting():Observable<Planting[]>{
        return this.httpClient.get<GetResponsePlanting>(this.baseUrl + this.baseUrlPlanting).pipe(
            map( response => response._embedded.plantings)
        )
    }

    getCalendaryTree(id:number):Observable<CalendaryTree[]>{
        return this.httpClient.get<GetResponseCalendary>(this.baseUrl + this.baseUrlTree+`s/${id}/calendary`).pipe(
            map( responce => responce._embedded.calendaryTrees)
        )
    }

    postMin(data:MinTree):Observable<MinTree>{
        return this.httpClient.post<PostResponseMinTree>(this.baseUrl + this.postMinTree,data).pipe(
            map( response => response.minCalendar)
        )
    }

    getMinCalendaryByCategory(id:number):Observable<MinTree[]>{
        return this.httpClient.get<GetResponseMinTree>(this.baseUrl + this.baseUrlByCategory+`/${id}/minCalendary`).pipe(
            map( response => response._embedded.treeCalendaryMins)
        )
    }

    deleteTree(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlTree + "/"+ id)
    }
    
    deleteCategory(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlCategory + "/" + id)
    }

    deleteCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `delete/calendar/tree/${id}`)
    }

    deleteMinCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `min/calendar/tree/${id}`)
    }

    updateCategory(data: Category){
        return this.httpClient.put(this.baseUrl + this.baseUrlCategory, data)
    }

    updateTree(data: Tree){
        return this.httpClient.put(this.baseUrl + this.baseUrlTree, data)
    }

    updateCalendar(data: CalendaryTree){
        return this.httpClient.post(this.baseUrl + this.baseUrlCalendaryTree, data)
    }

    updateMinCalendar(data: MinTree){
        return this.httpClient.post(this.baseUrl + this.postMinTree, data)
    }
}

interface GetResponseCategory{
    categorys: Category[];
}

interface GetResponseTree{
    _embedded:{
        trees:Tree[];
    }
}

interface PostResponseTree{
    tree: Tree;
}

interface GetResponsePlanting{
    _embedded:{
        plantings: Planting[];
    }
}

interface PostResponseMinTree{
    minCalendar: MinTree
}

interface GetResponseMinTree{
    _embedded:{
        treeCalendaryMins:MinTree[]
    }
}

interface GetResponseCalendary{
    _embedded:{
        calendaryTrees: CalendaryTree[]
    }
}

interface PostResponseCalendary{
    calendar: CalendaryTree
}