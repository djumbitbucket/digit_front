import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable,map } from "rxjs";
import { Bowl } from "src/commons/bowl";
import { Fish } from "src/commons/fish";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class BowlService{
    private baseUrl = environment.apiOut;

    private baseUrlBowls = "bowl";

    constructor(private httpClient: HttpClient){}

    getBowl():Observable<Bowl[]>{
        return this.httpClient.get<GetResponseBowl>(this.baseUrl + this.baseUrlBowls).pipe(
            map( response => response.bowls)
        )
    }

    getFishByBowl(id:number):Observable<Fish[]>{
        return this.httpClient.get<GetResponseFish>(this.baseUrl + this.baseUrlBowls+`s/${id}/fish`).pipe(
            map( response => response._embedded.fish)
        )
    }

    postBowl(data:Bowl){
        return this.httpClient.post(this.baseUrl + this.baseUrlBowls,data)
    }
    
    deleteBowl(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlBowls+`/${id}`)
    }

    updateBowl(data:Bowl){
        return this.httpClient.put(this.baseUrl + this.baseUrlBowls,data)
    }
}

interface GetResponseBowl{
    bowls: Bowl[]
}

interface GetResponseFish{
    _embedded:{
        fish: Fish[]
    }
}