import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable,map } from "rxjs";
import { Bowl } from "src/commons/bowl";
import { Fish } from "src/commons/fish";
import { IncomingStock } from "src/commons/IncomingStock";
import { OutgoingStock } from "src/commons/OutgoingStock ";
import { Product } from "src/commons/product";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class MatiereService{
    private baseUrl = environment.apiOut;

    private baseUrlMatiere = "product";
    private baseUrlIncomingStock = "incoming";

    constructor(private httpClient: HttpClient){}

    getProducts():Observable<any[]>{
        return this.httpClient.get<GetResponseProduct>(this.baseUrl + this.baseUrlMatiere).pipe(
            map( response => response.products)
        )
    }

    getIncomingStockByMatiere(product_id: number):Observable<IncomingStock[]>{
        return this.httpClient.get<GetResponseIncomingStock>(this.baseUrl + this.baseUrlIncomingStock + '/' + product_id).pipe(
            map( response => response.stocks)
        )
    }

    postProduct(data: Product){
        return this.httpClient.post(this.baseUrl + this.baseUrlMatiere,data)
    }
    
    deleteProduct(id: number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlMatiere +`/${id}`)
    }

    updateProduct(data: Product){
        return this.httpClient.put(this.baseUrl + this.baseUrlMatiere,data)
    }

    postIncomingStock(data: IncomingStock){
        return this.httpClient.post(this.baseUrl + this.baseUrlIncomingStock,data)
    }
    
    deleteIncomingStock(id: number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlIncomingStock +`/${id}`)
    }

    updateIncomingStock(data: IncomingStock){
        return this.httpClient.put(this.baseUrl + this.baseUrlIncomingStock,data)
    }
}

interface GetResponseProduct{
    products: Product[]
}

interface GetResponseIncomingStock{
    stocks: IncomingStock[]
}