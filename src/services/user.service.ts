import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { User } from '../commons/User';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';;
import { environment } from 'src/environments/environment';
import { Role } from 'src/commons/role';

@Injectable({
    providedIn: 'root'
  })
  export class UserService {

    private baseUrl = environment.apiOut;
    private baseUrlUser = "user";
    private baseUrlRole = "role";

    constructor(private httpClient: HttpClient) { }

    getRoles():Observable<Role[]>{
        return this.httpClient.get<GetResponseRole>(this.baseUrl + this.baseUrlRole).pipe(
            map( response => response.roles)
        )
    }
    
    getUser():Observable<User[]>{
        return this.httpClient.get<GetResponseUser>(this.baseUrl + this.baseUrlUser).pipe(
            map(
                response => response.users
            )
        )
    }

    postUser(data:User){
        return this.httpClient.post(this.baseUrl + this.baseUrlUser,data)
    }

    activation(id:number){
        return this.httpClient.get(this.baseUrl + this.baseUrlUser +"/"+id)
    }

    updateUser(data:User){
        return this.httpClient.put(this.baseUrl + this.baseUrlUser,data)
    }

  }

  interface GetResponseUser{
    users: User[]
  }

  interface GetResponseRole{
    roles: Role[]
  }
