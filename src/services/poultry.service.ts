import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ChickenCoop } from "src/commons/chickenCoop";
import { Poultry } from "src/commons/poultry";
import { map } from "rxjs";
import { CalendaryPoultry } from "src/commons/calendaryPoultry";
import { Category } from "src/commons/category";
import { MinPoultry } from "src/commons/minPoultry";
import { environment } from "src/environments/environment";
import { OutPoultry } from "src/commons/outPoultry";
import { CategoryPoultry } from "src/commons/CategoryPoultry";
import { Egg } from "src/commons/egg";

@Injectable({
    providedIn: 'root'
})

export class PoultryService{

    private baseUrl = environment.apiOut;
    private baseUrlChickenCoop = "chickenCoops";
    private baseUrlPoultry = "poultry";
    private baseUrlCategory = "poultryCategory";
    private baseUrlByCategory = "poultryCategories";
    private baseUrlCalendaryPoultry = "add/calendar/poultry";
    private postMinCalendar = "min/calendar/poultry";
    private baseUrlOutPoultry = "outPoultry";
    private baseUrlEgg = "egg";
    private baseUrlCalendarFrequency = "frequence/calendar/poultry";

    constructor(private httpClient:HttpClient){}

    getData():Observable<Poultry[]>{
        return this.httpClient.get<GetResponcePoultry>(this.baseUrl+"poultrys").pipe(
            map( responce => responce.poultrys)
        )
    }

    getChickenCoops():Observable<ChickenCoop[]>{
        return this.httpClient.get<GetResponceChickenCoop>(this.baseUrl + this.baseUrlChickenCoop).pipe(
            map( response => response._embedded.chickenCoops)
        )
    }

    getCategory():Observable<CategoryPoultry[]>{
        return this.httpClient.get<GetResponseCategory>(this.baseUrl + this.baseUrlCategory).pipe(
            map( response => response.categorys)
        )
    }

    postPoultry(data:Poultry){
        return this.httpClient.post(this.baseUrl+"poultry",data)
    }
    getCalendaryPoultry(id:number):Observable<CalendaryPoultry[]>{
        return this.httpClient.get<GetResponseCalendary>(this.baseUrl+`poultrys/${id}/calendary`).pipe(
            map( responce => responce._embedded.calendaryPoultries)
        )
    }

    getPoultryByCategory(id:number):Observable<Poultry[]>{
        return this.httpClient.get<GetResponcePoultry>(this.baseUrl + this.baseUrlPoultry+`/${id}`).pipe(
            map( response => response.poultrys)
        )
    }

    getMinCalendaryByCategory(id:number):Observable<MinPoultry[]>{
        return this.httpClient.get<GetResponseMinPoultry>(this.baseUrl + this.baseUrlByCategory+`/${id}/minCalendary`).pipe(
            map( response => response._embedded.poultryCalendaryMins)
        )
    }

    postCalendaryPoultry(data):Observable<CalendaryPoultry>{
        return this.httpClient.post<PostResponseCalendary>(this.baseUrl + this.baseUrlCalendaryPoultry,data).pipe(
            map( response => response.calendar)
        )

    }

    postCategory(data:Category){
        return this.httpClient.post(this.baseUrl + this.baseUrlCategory,data)
    }

    postMinPoultry(data:MinPoultry):Observable<MinPoultry>{
        return this.httpClient.post<PostResponseMinPoultry>(this.baseUrl+"min/calendar/poultry",data).pipe(
            map( response => response.minCalendar)
        )
    }

    deletePoultry(id:number){
        return this.httpClient.delete(this.baseUrl+"poultry/"+id)
    }
    
    deleteCategory(id:number){
        return this.httpClient.delete(this.baseUrl + `poultryCategory/${id}`)
    }

    deleteCalendar(id:number){
        return this.httpClient.delete(this.baseUrl + `delete/calendar/poultry/${id}`)
    }

    deleteMinCalendar(id:number){   //ne marche pas encore
        return this.httpClient.delete(this.baseUrl + `min/calendar/poultry/${id}`)
    }

    updateCategory(data: Category){
        return this.httpClient.put(this.baseUrl + this.baseUrlCategory, data)
    }

    updatePoultry(data: Poultry){
        return this.httpClient.put(this.baseUrl + this.baseUrlPoultry, data)
    }

    updateCalendar(data: any){
        return this.httpClient.post(this.baseUrl + this.baseUrlCalendaryPoultry, data)
    }

    updateMinCalendar(data: MinPoultry){
        return this.httpClient.post(this.baseUrl + this.postMinCalendar, data)
    }
    
    //sortie
    getOutPoultry(id_poultry: number):Observable<OutPoultry[]>{
        return this.httpClient.get<GetResponseOutPoultry>(this.baseUrl + this.baseUrlOutPoultry +"/" + id_poultry).pipe(
            map ( responce => responce.outPoultrys)
        )
    } 

    postOutPoultry(data: OutPoultry){
        return this.httpClient.post(this.baseUrl + this.baseUrlOutPoultry,data)
    }

    updateOutPoultry(data: OutPoultry){
        return this.httpClient.put(this.baseUrl + this.baseUrlOutPoultry,data)
    }

    deleteOutPoultry(id: number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlOutPoultry + "/" + id)
    }

    //egg
    getEgg(id_poultry: number):Observable<Egg[]>{
        return this.httpClient.get<GetResponseEgg>(this.baseUrl + this.baseUrlEgg +"/" + id_poultry).pipe(
            map ( responce => responce.eggs)
        )
    } 

    postEgg(data: Egg){
        return this.httpClient.post(this.baseUrl + this.baseUrlEgg, data)
    }

    updateEgg(data: Egg){
        return this.httpClient.put(this.baseUrl + this.baseUrlEgg, data)
    }

    deleteEgg(id: number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlEgg + "/" + id)
    }

    //calendarFrequency
    postCalendary2(data: any){
        return this.httpClient.post(this.baseUrl + this.baseUrlCalendarFrequency,data)
    }

}

interface GetResponcePoultry{
    poultrys:Poultry[]
}

interface GetResponceChickenCoop{
    _embedded:{
        chickenCoops:ChickenCoop[]
    }
}

interface PostResponce{
    poultry: Poultry
}

interface PostResponseCalendary{
    calendar: CalendaryPoultry
}

interface GetResponseCalendary{
    _embedded:{
        calendaryPoultries:CalendaryPoultry[]
    }
}

interface GetResponseCategory{
    categorys: CategoryPoultry[]
}

interface PostResponseMinPoultry{
    minCalendar: MinPoultry
}

interface GetResponseMinPoultry{
    _embedded:{
        poultryCalendaryMins:MinPoultry[]
    }
}

interface GetResponseOutPoultry{
    outPoultrys: OutPoultry[];
}

interface GetResponseEgg{
    eggs: Egg[];
}