import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { Planting } from "src/commons/planting";
import { Speculation } from "src/commons/speculation";
import { Tree } from "src/commons/tree";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class PlantingService {
    
    private baseUrl = environment.apiOut;

    private baseUrlPlanting = "planting";

    constructor(private httpClient: HttpClient){}

    getPlanting(){
        return this.httpClient.get<GetResponsePlanting>(this.baseUrl + this.baseUrlPlanting).pipe(
            map( response => response.plantings)
        )
    }

    getSpeculationByPlanting(id:number):Observable<Speculation[]>{
        return this.httpClient.get<GetResponseSpeculation>(this.baseUrl + this.baseUrlPlanting+`s/${id}/speculation`).pipe(
            map( response => response._embedded.speculations)
        )
    }

    getTreeByPlanting(id:number):Observable<Tree[]>{
        return this.httpClient.get<GetResponseTree>(this.baseUrl + this.baseUrlPlanting+`s/${id}/tree`).pipe(
            map( response => response._embedded.trees)
        )
    }

    postPlanting(data:Planting):Observable<Planting>{
        return this.httpClient.post<Planting>(this.baseUrl + this.baseUrlPlanting,data)
    }
    
    deletePlanting(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlPlanting+"/"+id)
    }

    updatePlanting(data:Planting):Observable<Planting>{
        return this.httpClient.put<Planting>(this.baseUrl + this.baseUrlPlanting,data)
    }
}

interface GetResponsePlanting{
    plantings: Planting[];
}

interface GetResponseSpeculation{
    _embedded:{
        speculations:Speculation[];
    }
}

interface GetResponseTree{
    _embedded:{
        trees:Tree[]
    }
}