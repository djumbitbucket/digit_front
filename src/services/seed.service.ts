import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable,map } from "rxjs";
import { Seed } from "src/commons/seed";
import { Speculation } from "src/commons/speculation";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class SeedService{
    
    private baseUrl = environment.apiOut;
    private baseUrlSeed = "seeds";

    constructor(private httpClient: HttpClient){}

    getSeed():Observable<Seed[]>{
        return this.httpClient.get<GetResponseSeed>(this.baseUrl + this.baseUrlSeed).pipe(
            map( response => response._embedded.seeds)
        )
    }

    getSpeculation(id:number):Observable<Speculation[]>{
        return this.httpClient.get<GetResponseSpeculation>(this.baseUrl + this.baseUrlSeed+`/${id}/speculation`).pipe(
            map( response => response._embedded.speculations)
        )
    }

    postSeed(data:Seed):Observable<Seed>{
        return this.httpClient.post<Seed>(this.baseUrl + this.baseUrlSeed,data)
    }
}

interface GetResponseSeed{
    _embedded:{
        seeds: Seed[]
    }
}

interface GetResponseSpeculation{
    _embedded:{
        speculations: Speculation[]
    }
}