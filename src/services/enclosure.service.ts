import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Enclosure } from '../commons/enclosure';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { Cattle } from 'src/commons/cattle';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
  })
  export class EnclosureService {

    private baseUrl = environment.apiOut;
    private baseUrlEnclosure = "enclosure";

    constructor(private httpClient: HttpClient) { }
    
    getEnclosure():Observable<Enclosure[]>{
        return this.httpClient.get<GetResponseEnclosure>(this.baseUrl + this.baseUrlEnclosure).pipe(
            map(
                response => response.enclosures
            )
        )
    }

    postEnclosure(data:Enclosure){
        return this.httpClient.post(this.baseUrl + this.baseUrlEnclosure,data)
    }

    getCattle(id:number):Observable<Cattle[]>{
        return this.httpClient.get<GetResponseCattle>(this.baseUrl + this.baseUrlEnclosure+`s/${id}/cattle`).pipe(
            map( response => response._embedded.cattles)
        )
    }

    deleteEnclosure(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlEnclosure +"/"+id)
    }

    updateEnclosure(data:Enclosure){
        return this.httpClient.put(this.baseUrl + this.baseUrlEnclosure,data)
    }

  }

  interface GetResponseEnclosure{
    enclosures: Enclosure[]
  }

  interface GetResponseCattle{
      _embedded:{
          cattles: Cattle[]
      }
  }