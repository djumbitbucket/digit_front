import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ChickenCoop } from "src/commons/chickenCoop";
import { map } from "rxjs";
import { Poultry } from "src/commons/poultry";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class ChickenCoopService{

    private baseUrl = environment.apiOut;
    private baseUrlChickenKoops = "chickenCoop";

    constructor(private httpClient: HttpClient){}

    getData():Observable<ChickenCoop[]>{
        return this.httpClient.get<GetResponseChickenCoop>(this.baseUrl + this.baseUrlChickenKoops).pipe(
           map(
               response => response.chickenCoops
           ) 
        )
    }

    postChickenCoop(data:ChickenCoop):Observable<ChickenCoop>{
        return this.httpClient.post<ChickenCoop>(this.baseUrl + this.baseUrlChickenKoops,data)
    }

    getPoultry(id:number):Observable<Poultry[]>{
        return this.httpClient.get<GetResponsePoultry>(this.baseUrl + this.baseUrlChickenKoops+`s/${id}/poultry`).pipe(
            map( response => response._embedded.poultrys)
        )
    }

    deleteChickenCoops(id:number){
        return this.httpClient.delete(this.baseUrl+this.baseUrlChickenKoops+"/"+id)
    }

    updateChickenCoop(data:ChickenCoop):Observable<ChickenCoop>{
        return this.httpClient.put<ChickenCoop>(this.baseUrl + this.baseUrlChickenKoops,data)
    }

}

interface GetResponseChickenCoop{
    chickenCoops:ChickenCoop[]
}

interface GetResponsePoultry{
    _embedded:{
        poultrys:Poultry[]
    }
}