import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Shop } from '../commons/Shop';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { User } from 'src/commons/user';
import { environment } from 'src/environments/environment';
import { Cashier } from 'src/commons/Cashier';

@Injectable({
    providedIn: 'root'
  })
  export class ShopService {

    private baseUrl = environment.apiOut;
    private baseUrlShop = "shop";
    private baseUrlCashier = "cashier";

    constructor(private httpClient: HttpClient) { }
    
    getShop():Observable<Shop[]>{
        return this.httpClient.get<GetResponseShop>(this.baseUrl + this.baseUrlShop).pipe(
            map(
                response => response.shops
            )
        )
    }

    postShop(data: Shop){
        return this.httpClient.post(this.baseUrl + this.baseUrlShop,data)
    }

    getUsersByShop(id:number):Observable<User[]>{
        return this.httpClient.get<GetResponseUser>(this.baseUrl + this.baseUrlShop+`/cashier/${id}`).pipe(
            map( response => response.users)
        )
    }

    deleteShop(id:number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlShop +"/"+id)
    }

    updateShop(data:Shop){
        return this.httpClient.put(this.baseUrl + this.baseUrlShop,data)
    }

    postCashier(data: Cashier){
        return this.httpClient.post(this.baseUrl + this.baseUrlCashier,data)
    }

  }

  interface GetResponseShop{
    shops: Shop[]
  }

  interface GetResponseUser{
    users: User[]
  }