import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable,map } from "rxjs";
import { Bowl } from "src/commons/bowl";
import { Fish } from "src/commons/fish";
import { OutgoingStock } from "src/commons/OutgoingStock ";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class StockService{
    private baseUrl = environment.apiOut;

    private baseUrlStockProsuit = "outgoing";

    constructor(private httpClient: HttpClient){}

    getStockProduits():Observable<any[]>{
        return this.httpClient.get<GetResponseStockProduit>(this.baseUrl + this.baseUrlStockProsuit + '/produit').pipe(
            map( response => response.stocks)
        )
    }

    getOutgoingStockByStockProduit(produit: string):Observable<OutgoingStock[]>{
        return this.httpClient.get<GetResponseOutgoingStock>(this.baseUrl + this.baseUrlStockProsuit + '/' + produit).pipe(
            map( response => response.outgoingStocks)
        )
    }

    postOutGoingStock(data: OutgoingStock){
        return this.httpClient.post(this.baseUrl + this.baseUrlStockProsuit,data)
    }
    
    deleteOutGoingStock(id: number){
        return this.httpClient.delete(this.baseUrl + this.baseUrlStockProsuit +`/${id}`)
    }

    updateOutGoingStock(data: OutgoingStock){
        return this.httpClient.put(this.baseUrl + this.baseUrlStockProsuit,data)
    }
}

interface GetResponseStockProduit{
    stocks: any[]
}

interface GetResponseOutgoingStock{
    outgoingStocks: OutgoingStock[]
}