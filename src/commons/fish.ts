import { BaseModel } from "./bases";
import { Bowl } from "./bowl";
import { Category } from "./category";

export class Fish extends BaseModel{

    public name: String;

    public quantity: number;

    public date: Date;

    public present: boolean;

    public category: Category;

    public categoryName: string;

    public bowl: Bowl;

    public bowlName: string;
	
	constructor(){		
		super();
        this.name = null;
        this.quantity = null;
        this.date = null;
        this.present = true;
        this.category = null;
        this.categoryName =null;
        this.bowl = null;
        this.bowlName = null;
	}

    edit(fish: Fish){
		super.edit(fish);
        this.name = fish.name;
        this.quantity = fish.quantity;
        this.date = fish.date;
        this.present = fish.present;
        this.category = fish.category;
        this.categoryName = fish.categoryName;
        this.bowl = fish.bowl;
        this.bowlName = fish.bowlName;
    }

}