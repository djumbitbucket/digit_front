import { CategoryCattle } from "./categoryCattle";
import { MinCalendar } from "./min-calendar";

export class MinCattle extends MinCalendar{

    public category: CategoryCattle;
	
	constructor(){		
		super();
		this.category = null;
	}

    edit(minCattle: MinCattle){
		super.edit(minCattle);
		this.category = minCattle.category;
    }

}