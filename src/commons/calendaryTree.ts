import { Calendar } from "./calendar";
import { Tree } from "./tree";

export class CalendaryTree extends Calendar{

    public tree: Tree
	
	constructor(){		
		super();
		this.tree = null;
	}

    edit(calendaryTree: CalendaryTree){
		super.edit(calendaryTree);
		this.tree = calendaryTree.tree;
    }
}