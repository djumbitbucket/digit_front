import { BaseModel } from "./bases";

export class Bowl extends BaseModel {
	
	public name : String;
	
	public area: number;
	
	public depth: number;

	public free: boolean;
	
	constructor(){		
		super();
		this.name = null;
		this.area = null;
		this.depth = null;
		this.free = true;
	}

    edit(bowl: Bowl){
		super.edit(bowl);
		this.name = bowl.name;
		this.area = bowl.area;
		this.depth = bowl.depth;
		this.free = bowl.free;
    }

}
