import { BaseModel } from "./bases";
import { Shop } from "./shop";
import { User } from "./user";

export class Cashier extends BaseModel{
	
	public active: boolean;
	public acess: string;
	public shop: Shop;
	public user: User;
	
	constructor(){
		super();
		this.active = true;
		this.acess = null;
		this.shop = null;
		this.user = null;
	}

    edit(cachier: Cashier){
		super.edit(cachier);
        this.active = cachier.active;
        this.acess = cachier.acess;
        this.shop = cachier.shop;
        this.user = cachier.user;
    }

}
