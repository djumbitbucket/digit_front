import { BaseModel } from "./bases";

export class MinCalendar extends BaseModel{

    public name: String;

    public intervention: String;

    public old: number;
	
	constructor(){		
		super();
		this.name = null;
		this.intervention = null;
		this.old = null;
	}

    edit(minCal: MinCalendar){
		super.edit(minCal);
		this.name = minCal.name;
		this.intervention = minCal.intervention;
		this.old = minCal.old;
    }

}