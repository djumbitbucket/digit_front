import { BaseModel } from "./bases";
import { Role } from "./role";

export class User extends BaseModel {
	
	public firstname: string;
	
	public lastname: string;
	
	public username: string;
	
	public password: string;

	public active: boolean;

	public role: Role;
	
	public role_id: number;		//utilisé pour formulaire
	
	constructor(){
		super();
		this.firstname = null;
		this.lastname = null;
		this.username = null;	//généré au backend
		this.password = null;
		this.active = true;
		this.role = null;
		this.role_id = 0;
	}

    edit(user: User){
		super.edit(user);
        this.firstname = user.firstname;
        this.lastname = user.lastname;
        this.username = user.username; 
        this.password = null;
        this.active = user.active;
        this.role = user.role;
        this.role_id = user.role.id;
    }

}
