import { Calendar } from "./calendar";
import { Fish } from "./fish";

export class CalendaryFish extends Calendar{

    public fish: Fish
	
	constructor(){		
		super();
		this.fish = null;
	}

    edit(calendaryFish: CalendaryFish){
		super.edit(calendaryFish);
		this.fish = calendaryFish.fish;
    }

}