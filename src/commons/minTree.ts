import { Category } from "./category";
import { MinCalendar } from "./min-calendar";

export class MinTree extends MinCalendar{

    public category: Category;
	
	constructor(){		
		super();
        this.category = null;
	}

    edit(minTree: MinTree){
		super.edit(minTree);
        this.category = minTree.category;
    }

}