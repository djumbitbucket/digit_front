import { Calendar } from "./calendar";
import { Poultry } from "./poultry";

export class CalendaryPoultry extends Calendar{

	public name : string;	//on create

	public frequence : number;	//on create

    public start : number;	//on create

    public end : number;	//on create

    public poultry : Poultry
	
	constructor(){		
		super();
		this.frequence = null;
		this.start = null;
		this.end = null;
		this.poultry = null;
	}

    edit(calendaryPoultry: CalendaryPoultry){
		super.edit(calendaryPoultry);
		this.poultry = calendaryPoultry.poultry;
    }
}