import { BaseModel } from "./bases";

export class Role extends BaseModel {
	
	public roleName: String;
	
	constructor(){		
		super();
		this.roleName = null;
	}

    edit(role: Role){
		super.edit(role);
		this.roleName = role.roleName;
    }

}
