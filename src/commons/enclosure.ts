import { BaseModel } from "./bases";

export class Enclosure extends BaseModel{
	
	public name: String;
	
	public area: number;
	
	constructor(){
		super();
		this.name = "";
		this.area = null;
	}

    edit(enclosure: Enclosure){
		super.edit(enclosure);
        this.name = enclosure.name;
        this.area = enclosure.area;
    }

}