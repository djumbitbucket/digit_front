export class BaseModel{

    public id : number;
	
	public createdOn: Date;
	
	public updatedOn: Date;
    
    constructor(){
        this.id = null;
        this.createdOn = new Date();
        this.updatedOn = new Date();
    }

    edit(model: any){
        this.id = model.id;
        this.createdOn = model.createdOn;
        this.updatedOn = new Date();
    }
}