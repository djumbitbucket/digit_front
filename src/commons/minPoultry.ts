import { Category } from "./category";
import { MinCalendar } from "./min-calendar";

export class MinPoultry extends MinCalendar{

    public category: Category;
	
	constructor(){		
		super();
		this.category = null;
	}

    edit(minPoultry: MinPoultry){
		super.edit(minPoultry);
		this.category = minPoultry.category;
    }

}