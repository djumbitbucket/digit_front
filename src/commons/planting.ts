import { BaseModel } from "./bases";
import { Seed } from "./seed";

export class Planting extends BaseModel {

    public name: String;

    public area: number;

    public free: boolean
    
    constructor(){
        super();
        this.name = null;
        this.area = null;
        this.free =  true;
    }

    edit(planting: Planting){
        super.edit(planting);
        this.name = planting.name;
        this.area = planting.area;
        this.free = planting.free;
    }
}