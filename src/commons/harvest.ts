import { BaseModel } from "./bases";
import { Speculation } from "./speculation";

export class Harvest extends BaseModel{
	
	public raison: string;
	
	public date: Date;

	public quantity: number;
	
	public valeur: number;

	public speculation: Speculation;
    
    constructor(){
		super();
		this.raison = 'sale';
		this.date = null;
		this.quantity = 0;
		this.valeur = 0;
		this.speculation = null;
    }

    edit(harvest: Harvest){
		super.edit(harvest);
        this.raison = harvest.raison;
        this.date = harvest.date;
        this.quantity = harvest.quantity;
        this.valeur = harvest.valeur;
        this.speculation = harvest.speculation;
    }
	
}