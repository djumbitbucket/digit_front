import { MinCalendar } from "./min-calendar";
import { Seed } from "./seed";

export class MinSpeculation extends MinCalendar {

    public seed: Seed;
	
	constructor(){		
		super();
		this.seed = null;
	}

    edit(minSpeculation: MinSpeculation){
		super.edit(minSpeculation);
		this.seed = minSpeculation.seed;
    }

}