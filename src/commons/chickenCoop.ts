import { BaseModel } from "./bases";

export class ChickenCoop extends BaseModel {
	
	public name: String;
	
	public area: number;

	public free: boolean
	
	constructor(){		
		super();
		this.name = null;
		this.area = null;
		this.free = true;
	}

    edit(chickenCoop: ChickenCoop){
		super.edit(chickenCoop);
		this.name = chickenCoop.name;
		this.area = chickenCoop.area;
		this.free = chickenCoop.free;
    }
	
}