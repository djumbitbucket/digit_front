import { BaseModel } from "./bases";
import { Category } from "./category";
import { Planting } from "./planting";

export class Tree extends BaseModel{

    public id: number;

    public name: String;

    public plantingDate: Date;

    public present: boolean;
	
	public createdOn: Date;
	
	public updatedOn: Date;

    public category: Category;

    public categoryName: String;

    public planting: Planting;

    public plantingName: String;
    
    constructor(){
        super();
        this.name = null;
        this.category = null;
        this.categoryName = null;
        this.present  = true;
        this.plantingDate = null;
        this.planting = null;
        this.plantingName = null;
    }

    edit(tree: Tree){
        super.edit(tree);
        this.name = tree.name;
        this.category = tree.category;
        this.categoryName = tree.categoryName;
        this.present = tree.present;
        this.plantingDate = tree.plantingDate;
        this.planting = tree.planting;
        this.plantingName = tree.plantingName;
    }

}