import { BaseModel } from "./bases";
import { Product } from "./product";

export class IncomingStock extends BaseModel {
	
	public type: string;    // in  | out
	
	public quantity: number;
	
	public date: Date;
	
	public product: Product;

	constructor(){		
		super();
        this.type = 'in';
        this.quantity = null;
        this.date = null;
        this.product = null;
	}

    edit(incomingStock: IncomingStock){
		super.edit(incomingStock);
        this.type = incomingStock.type;
        this.quantity = incomingStock.quantity;
        this.date = incomingStock.date;
        this.product = incomingStock.product;
    }
}