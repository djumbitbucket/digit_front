import { BaseModel } from "./bases";

export class Calendar extends BaseModel{
	
	public calendaryName: string;
	
	public intervention: string;
	
	public make: boolean;
	
	public date: Date;

	public giveUp: boolean;

	public description: string;
	
	constructor(){		
		super();
		this.calendaryName = null;
		this.intervention = null;
		this.make = false;
		this.date = new Date();
		this.giveUp = false;
		this.description = null;
	}

    edit(calendar: Calendar){
		super.edit(calendar);
		this.calendaryName = calendar.calendaryName;
		this.intervention = calendar.intervention;
		this.make = calendar.make;
		this.date = calendar.date;
    }

}