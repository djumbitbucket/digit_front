import { BaseModel } from "./bases";

export class Seed extends BaseModel {
	
	public seedName: String;
	
	constructor(){		
		super();
		this.seedName = null;
	}

    edit(seed: Seed){
		super.edit(seed);
		this.seedName = seed.seedName;
    }

}