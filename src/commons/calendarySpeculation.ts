import { Calendar } from "./calendar";
import { Speculation } from "./speculation";

export class CalendarySpeculation extends Calendar{

	public name : string;	//on create

	public frequence : number;	//on create

    public start : number;	//on create

    public end : number;	//on create

    public speculation : Speculation
	
	constructor(){		
		super();
		this.frequence = null;
		this.start = null;
		this.end = null;
		this.speculation = null;
	}

    edit(calendarySpeculation: CalendarySpeculation){
		super.edit(calendarySpeculation);
		this.speculation = calendarySpeculation.speculation;
    }
}