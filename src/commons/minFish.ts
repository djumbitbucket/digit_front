import { Category } from "./category";
import { MinCalendar } from "./min-calendar";

export class MinFish extends MinCalendar{

    public category: Category;
	
	constructor(){		
		super();
		this.category = null;
	}

    edit(minFish: MinFish){
		super.edit(minFish);
		this.category = minFish.category;
    }

}