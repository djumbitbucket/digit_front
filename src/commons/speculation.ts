import { BaseModel } from "./bases";
import { Planting } from "./planting";
import { Seed } from "./seed";

export class Speculation extends BaseModel {

	public id: number;
	
	public name: String;
	
	public seedDate: Date;

	public present: boolean;
	
	public createdOn: Date;
	
	public updatedOn: Date;

	public seedName: String;

    public seed: Seed;

    public planting: Planting;

	public plantingName: String
	
	constructor(){		
		super();
        this.name = '';
        this.seedName = '';
        this.seedDate = null;
        this.present = true;
        this.seed = null;
        this.planting = null;
        this.plantingName = null;
	}

    edit(speculation: Speculation){
		super.edit(speculation);
        this.name = speculation.name;
        this.seedName = speculation.seedName;
        this.seedDate = speculation.seedDate;
        this.present = speculation.present;
        this.seed = speculation.seed;
        this.planting = speculation.planting;
        this.plantingName = speculation.plantingName;
    }

}