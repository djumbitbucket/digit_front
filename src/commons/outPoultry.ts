import { BaseModel } from "./bases";
import { Poultry } from "./poultry";

export class OutPoultry extends BaseModel{
	
	public raison: string;
	
	public date: Date;
	
	public quantity: number;
	
	public valeur: number;

    public poultry: Poultry;
	
	constructor(){		
		super();
		this.raison = 'sale';
		this.date = null;
		this.quantity = null;
		this.valeur = null;
		this.poultry = null;
	}

    edit(outPoultry: OutPoultry){
		super.edit(outPoultry);
		this.raison = outPoultry.raison;
		this.date = outPoultry.date;
		this.quantity = outPoultry.quantity;
		this.valeur = outPoultry.valeur;
		this.poultry = outPoultry.poultry;
    }

}