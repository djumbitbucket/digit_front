import { BaseModel } from "./bases";

export class Shop extends BaseModel{
	
	public name: string;
	
	constructor(){
		super();
		this.name = null;
	}

    edit(shop: Shop){
		super.edit(shop);
        this.name = shop.name;
    }

}
