import { BaseModel } from "./bases";

export class Category extends BaseModel{
    
    public name: String;
	
	constructor(){		
		super();
		this.name = null;
	}

    edit(category: Category){
		super.edit(category);
		this.name = category.name;
    }
    
}