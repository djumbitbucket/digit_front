import { BaseModel } from "./bases";
import { Fish } from "./fish";
import { Speculation } from "./speculation";

export class CalendarFrequency extends BaseModel{ //plus utilisé

    public intervention : string;

    public frequence : number;

    public start : number;

    public end : number;

    public fish : Fish;

    public speculation : Speculation;
	
	constructor(){		
		super();
		this.intervention = null;
		this.frequence = null;
		this.start = null;
		this.end = null;
		this.fish = null;
		this.speculation = null;
	}

    edit(calendarFrequency: CalendarFrequency ){
		super.edit(calendarFrequency);
		this.intervention = calendarFrequency.intervention;
		this.frequence = calendarFrequency.frequence;
		this.start = calendarFrequency.start;
		this.end = calendarFrequency.end;
		this.fish = calendarFrequency.fish;
		this.speculation = calendarFrequency.speculation;
    }

}