import { BaseModel } from "./bases";
import { Poultry } from "./poultry";

export class OutgoingStock extends BaseModel {
	
	public quantity: number;
	
	public valeur: number;
	
	public type: string;
	
	public produit: string;
	
	public subjectId: string;

	constructor(){		
		super();
        this.quantity = null;
        this.valeur = null;
        this.type = 'out';
        this.produit = null;
        this.subjectId = null;
	}

    edit(out: OutgoingStock){
		super.edit(out);
        this.quantity = out.quantity;
        this.valeur = out.valeur;
        this.type = 'out';
        this.produit = out.produit;
        this.subjectId = out.subjectId;
    }

}