import { Category } from "./category";

export class CategoryPoultry extends Category{

    public type: string;
	
	constructor(){		
		super();
		this.type = 'pulpit'; //chaire, oeuf feconde, oeuf non feconde //pulpit=chaire, fertil=feconde et not_fertil = non feconde
	}

    edit(categoryPoultry: CategoryPoultry){
		super.edit(categoryPoultry);
		this.type = categoryPoultry.type;
    }
    
}