import { Category } from "./category";

export class CategoryCattle extends Category{

    public family: String;
	
	constructor(){		
		super();
		this.family = null;
	}

    edit(categoryCattle: CategoryCattle){
		super.edit(categoryCattle);
		this.family = categoryCattle.family;
    }
    
}