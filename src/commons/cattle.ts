import { BaseModel } from "./bases";
import { Category } from "./category";
import { Enclosure } from "./enclosure";

export class Cattle extends BaseModel{
	
	public name: String;

	public family: String;
	
	public gender: String;
	
	public present: boolean;
	
	public date: Date;

	public enclosure: Enclosure;

	public enclosureName: string;
	
	public category: Category;

	public categoryName: string
	
	constructor(){		
		super();
		this.category = null;
		this.categoryName = null;
		this.date = null;
		this.enclosure = null;
		this.enclosureName = null;
		this.family = null;
		this.gender = "Male";
		this.name = null;
		this.present = true;
	}

    edit(cattle: Cattle){
		super.edit(cattle);
		this.category = cattle.category;
		this.categoryName = cattle.categoryName;
		this.date = cattle.date;
		this.enclosure = cattle.enclosure;
		this.enclosureName = cattle.enclosureName;
		this.family = cattle.family;
		this.gender = cattle.gender;
		this.name = cattle.name;
		this.present = cattle.present;
    }
}