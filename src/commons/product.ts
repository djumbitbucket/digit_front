import { BaseModel } from "./bases";

export class Product extends BaseModel {
	
	public productName: string;

	constructor(){		
		super();
        this.productName = null;
	}

    edit(matiere: Product){
		super.edit(matiere);
        this.productName = matiere.productName;
    }
}