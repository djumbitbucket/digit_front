export class LoggerValue{

  public  firstname : string ;

  public  lastname : string ;

  public  username : string ;

  public  token : string ;

  public  role : string ;
}