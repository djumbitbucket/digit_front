import { BaseModel } from "./bases";
import { Poultry } from "./poultry";

export class Egg extends BaseModel{
	
	public destination: string;
	
	public quantity: number;

    public poultry: Poultry;
	
	constructor(){		
		super();
		this.destination = 'stock';	//stock | sale
		this.quantity = null;
		this.poultry = null;
	}

    edit(egg: Egg){
		super.edit(egg);
		this.destination = egg.destination;
		this.quantity = egg.quantity;
		this.poultry = egg.poultry;
    }

}