import { Calendar } from "./calendar";
import { Cattle } from "./cattle";

export class CalendaryCattle extends Calendar{

    public cattle : Cattle
	
	constructor(){		
		super();
		this.cattle = null;
	}

    edit(calendaryCattle: CalendaryCattle){
		super.edit(calendaryCattle);
		this.cattle = calendaryCattle.cattle;
    }

}

