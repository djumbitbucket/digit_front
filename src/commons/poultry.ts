import { BaseModel } from "./bases";
import { Category } from "./category";
import { ChickenCoop } from "./chickenCoop";

export class Poultry extends BaseModel{
	
	public id: number;
	
	public name: String;
	
	public developmentTime: number;
	
	public dateOfEntry: Date;
	
	public initQuantity: number;

	public quantity: number;
	
	public present: Boolean;
	
	public createdOn: Date;
	
	public updatedOn: Date;

	public chickenCoop: ChickenCoop;

	public category: Category;

	public categoryName: string;

	public coopsName: string;
	
	constructor(){		
		super();
		this.name = null;
        this.developmentTime = null;
        this.initQuantity = null;
        this.quantity = null;
        this.present = true;
        this.chickenCoop = null;
        this.category = null;
        this.categoryName = null;
        this.coopsName = null;
        this.dateOfEntry = null;
	}

    edit(poultry: Poultry){
		super.edit(poultry);
		this.name = poultry.name;
		this.developmentTime = poultry.developmentTime;
		this.initQuantity = poultry.initQuantity;
		this.quantity = poultry.quantity;
		this.present = poultry.present;
		this.category = poultry.category;
		this.categoryName = poultry.categoryName;
		this.chickenCoop = poultry.chickenCoop;
		this.coopsName = poultry.coopsName;
		this.dateOfEntry = poultry.dateOfEntry;
    }

}