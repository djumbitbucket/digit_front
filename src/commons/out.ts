export class Out {
	
	public id: number;
	
	public raison: String;
	
	public date: Date;
	
	public quantity: number;

	public createdOn: Date;
	
	public updatedOn: Date;
}